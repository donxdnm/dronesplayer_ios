//
//  changePWViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 15/8/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import CDAlertView
import NVActivityIndicatorView

class ChangePWViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var changePWContrainerView: UIView!
    @IBOutlet weak var oldPWTV: UITextField!
    @IBOutlet weak var pw1TV: UITextField!
    @IBOutlet weak var pw2TV: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem()
        backButton.title =  NSLocalizedString("返回", comment: "") //in your case it will be empty or you can put the title of your choice
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func changePWBtnClicked(_ sender: Any) {
        if pw1TV.text != pw2TV.text {
            let errorAlert =  CDAlertView(title:"", message: NSLocalizedString("兩次輸入的密碼不同，請再試一次", comment: ""), type: .notification)
            errorAlert.show()
            return
        }
        startAnimating()
        Functions.changePWapi(oldPW: oldPWTV.text!, newPW: pw1TV.text!, complete: changePWCallback(success:message:))
    }
    
    func changePWCallback(success:Bool,message:String){
        self.stopAnimating()
        if success{
            let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
            let okAction = CDAlertViewAction.init(title: NSLocalizedString("好", comment: ""), font: UIFont.systemFont(ofSize: 20), textColor: .black, backgroundColor: .white, handler: {(action:CDAlertViewAction) in
                self.navigationController?.popViewController(animated: true)
                return true
            })
            errorAlert.add(action: okAction)
            errorAlert.show()
        }else{
            if (Constants.defaults.has(Constants.accessTokenKey)){
                Functions.accessFailCallback(actionBeforeMessageShow: { (autoLoginSuccess:Bool) in
                    if !autoLoginSuccess{
                    self.navigationController?.popToRootViewController(animated: true)
                    }
                })
            }else{
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
                errorAlert.show()
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
