//
//  ResetPwViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 14/8/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CDAlertView

class ResetPwViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var resetContrainerView: UIView!
    @IBOutlet weak var userIdTV: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetBtnClicked(_ sender: UIButton) {
        startAnimating()
        Functions.resetPwApi(userName: userIdTV.text, complete: {(success:Bool, message:String) -> Void in
            if success{
                self.stopAnimating()
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message , type: .notification)
                let okAction = CDAlertViewAction(title: NSLocalizedString("OK", comment: ""), font: UIFont.systemFont(ofSize: 20) , textColor: .black, backgroundColor: .white, handler: {(action:CDAlertViewAction)-> Bool in
                    self.navigationController?.popViewController(animated: true)
                    return true
                })
                errorAlert.add(action: okAction)
                errorAlert.show()
                
            }else{
                self.stopAnimating()
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
                errorAlert.show()
            }
            
        })
        
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
