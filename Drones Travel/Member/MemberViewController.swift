//
//  MemberViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 13/8/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Spring
import FontAwesome_swift
import NVActivityIndicatorView
import CDAlertView

class MemberViewController: UIViewController ,NVActivityIndicatorViewable{

    enum segue :String{
        case toIndex = "goToIndex"
        case toSetting = "goToSetting"
    }

    @IBOutlet weak var loginContrainerView: UIView!
    @IBOutlet weak var mailIconIV: UIImageView!
    @IBOutlet weak var pwIconIV: UIImageView!
    @IBOutlet weak var loginTV: UITextField!
    @IBOutlet weak var pwTV: UITextField!
    @IBOutlet weak var skipBtn: UIButton!
    
    var segueIDAfterLogin = segue.toIndex

    override func viewDidLoad() {
        super.viewDidLoad()
        mailIconIV.image = UIImage.fontAwesomeIcon(name: .envelope, style: .solid, textColor: .gray, size: CGSize(width: 30, height: 30))
        pwIconIV.image = UIImage.fontAwesomeIcon(name: .unlockAlt, style: .solid, textColor: .gray, size: CGSize(width: 30, height: 30))
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: NSLocalizedString("登入中⋯⋯", comment: ""), type: NVActivityIndicatorType(rawValue: 6)!)
        Functions.loginApi(accessToken: nil, userName: loginTV.text, password: pwTV.text, useAccessToken: false, complete: {(success:Bool, message:String) -> Void in
            if success{
                self.stopAnimating()
                if self.segueIDAfterLogin == segue.toSetting{
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.performSegue(withIdentifier: self.segueIDAfterLogin.rawValue, sender: nil)
                }
                
                
            }else{
                self.stopAnimating()
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
                errorAlert.show()
            }
            
        })
    }
    
    @IBAction func skipBtnClicked(_ sender: UIButton) {
        if segueIDAfterLogin == .toSetting{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.performSegue(withIdentifier: "goToIndex", sender: nil)
        }
        
    }
    
    @IBAction func registerBtnClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToRegister", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        if identifier == MemberViewController.segue.toIndex.rawValue{
            if (Constants.countryAr.count) > 0{
               return true
            }else{
                self.showNetworkError()
                NotificationCenter.default.addObserver(self, selector: #selector(self.goToIndex), name: NSNotification.Name(rawValue: "countryArReady"), object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(self.showNetworkError), name: NSNotification.Name(rawValue: "countryArFetchFail"), object: nil)
                return false
            }
        }
        return true
    }
 
    @objc func goToIndex(){
        self.performSegue(withIdentifier: segue.toIndex.rawValue, sender: self)
    }
    
    @objc func showNetworkError(){
        let errorAlert = Functions.showNetworkErrorAlert {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getCountryApi()
        }
        errorAlert.show()
    }
}
