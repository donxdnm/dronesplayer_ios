//
//  ProfileViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 14/8/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CDAlertView

class ProfileViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var memberPicIV: UIImageView!
    @IBOutlet weak var profileEditBtn: UIButton!
    @IBOutlet weak var userNameTV: UITextField!
    @IBOutlet weak var emailTV: UITextField!
    @IBOutlet weak var phoneTV: UITextField!
    @IBOutlet weak var displayNameLB: UILabel!
    @IBOutlet weak var genderLB: UILabel!
    @IBOutlet weak var contrainerView: UIView!
    @IBOutlet weak var confirmEditBtnContrainerView: UIView!
    var profile:ProfileData?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        startAnimating()
        Functions.fetchProfileApi(complete: fetchProfileCallback(success:message:data:))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func fetchProfileCallback(success:Bool, message:String, data:ProfileData?){
        if success{
            self.stopAnimating()
            profile = data
            displayProfile(profile: profile)
            
        }else{
            self.stopAnimating()
            if (Constants.defaults.has(Constants.accessTokenKey)){
                Functions.accessFailCallback(actionBeforeMessageShow: { (autoLoginSuccess:Bool) in
                    self.navigationController?.popViewController(animated: true)
                })
            }else{
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
                errorAlert.show()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func editProfileCallback(success:Bool, message:String, data:ProfileData?){
        if success{
            self.stopAnimating()
            profile = data
            displayProfile(profile: profile)
            
        }else{
            self.stopAnimating()
            if (Constants.defaults.has(Constants.accessTokenKey)){
                Functions.accessFailCallback(actionBeforeMessageShow: { (autoLoginSuccess:Bool) in
                    if !autoLoginSuccess{
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            }else{
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
                errorAlert.show()
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func profileEditBtnCicked(_ sender: Any?) {
        if userNameTV.isUserInteractionEnabled{
            userNameTV.isUserInteractionEnabled = false
            userNameTV.borderStyle = .none
            userNameTV.backgroundColor = .clear
            emailTV.isUserInteractionEnabled = false
            emailTV.borderStyle = .none
            emailTV.backgroundColor = .clear
            phoneTV.isUserInteractionEnabled = false
            phoneTV.borderStyle = .none
            phoneTV.backgroundColor = .clear
            profileEditBtn.isEnabled = true
            confirmEditBtnContrainerView.isHidden = true
            profileEditBtn.setImage(#imageLiteral(resourceName: "profile_edit_btn_icon"), for: .normal)
        }else{
            userNameTV.isUserInteractionEnabled = true
            userNameTV.borderStyle = .line
            userNameTV.backgroundColor = .white
            emailTV.isUserInteractionEnabled = true
            emailTV.borderStyle = .line
            emailTV.backgroundColor = .white
            phoneTV.isUserInteractionEnabled = true
            phoneTV.borderStyle = .line
            phoneTV.backgroundColor = .white
            profileEditBtn.isEnabled = false
            profileEditBtn.setImage(#imageLiteral(resourceName: "profile_editing_btn_icon"), for: .normal)
            confirmEditBtnContrainerView.isHidden = false
        }
    }
    
    @IBAction func emailModifyBtnClicked(_ sender: Any) {
    }
    @IBAction func phoneModifyBtnClicked(_ sender: Any) {
    }
    
    func setupView(){
        
        contrainerView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "profile_background"))
        memberPicIV.layer.borderWidth = 5
        memberPicIV.layer.masksToBounds = false
        memberPicIV.layer.borderColor = UIColor.white.cgColor
        memberPicIV.layer.cornerRadius = memberPicIV.frame.height / 2
        memberPicIV.clipsToBounds = true
        let iconSize = CGSize(width:30,height:30)
//        profileEditBtn.setImage(UIImage.fontAwesomeIcon(name: .edit, textColor: .white, size: iconSize), for: .normal)
        
        
        let backButton = UIBarButtonItem()
        backButton.title =  NSLocalizedString("返回", comment: "") //in your case it will be empty or you can put the title of your choice
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        userNameTV.isUserInteractionEnabled = false
        emailTV.isUserInteractionEnabled = false
        phoneTV.isUserInteractionEnabled = false
        confirmEditBtnContrainerView.isHidden = true
    }
    
    func displayProfile(profile:ProfileData?){
        self.displayNameLB.text = profile?.display_name ?? "沒有提供資料"
        self.emailTV.text = profile?.email ?? "沒有提供資料"
        self.phoneTV.text = profile?.mobile ?? "沒有提供資料"
        self.userNameTV.text = profile?.display_name ?? "沒有提供資料"
        switch (profile?.gender){
        case "m":
            self.genderLB.text = "男"
        case "f":
            self.genderLB.text = "女"
        default:
            self.genderLB.text = "沒有提供資料"
        }
    }
    @IBAction func reverseBtnClicked(_ sender: Any) {
        displayProfile(profile: profile)
        profileEditBtnCicked(nil)
    }
    @IBAction func submitEditBtnClicked(_ sender: Any) {
        startAnimating()
        Functions.updateProfileApi(displayName: userNameTV.text!, user_email: emailTV.text!, mobile: phoneTV.text!, complete: editProfileCallback(success:message:data:))
        profileEditBtnCicked(nil)
    }
    @IBAction func logoutBtnClicked(_ sender: Any) {
        Constants.defaults.clear(Constants.accessTokenKey)
        Constants.defaults.clear(Constants.memberIdKey)
        Functions.clearCookie()
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
