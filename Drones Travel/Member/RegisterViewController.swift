//
//  RegisterViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 14/8/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import CDAlertView
import NVActivityIndicatorView

class RegisterViewController: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var registerContrainerView: UIView!
    @IBOutlet weak var mailIconIV: UIImageView!
    @IBOutlet weak var pwIconIV: UIImageView!
    @IBOutlet weak var pw2IconIV: UIImageView!
    @IBOutlet weak var phoneIconIV: UIImageView!
    @IBOutlet weak var genderIconIV: UIImageView!
    @IBOutlet weak var incomeIconIV: UIImageView!
    @IBOutlet weak var userNameTV: UITextField!
    @IBOutlet weak var emailTV: UITextField!
    @IBOutlet weak var pwTV: UITextField!
    @IBOutlet weak var pw2TV: UITextField!
    @IBOutlet weak var phoneTV: UITextField!
    @IBOutlet weak var ageSC: UISegmentedControl!
    @IBOutlet weak var genderSC: UISegmentedControl!
    let genderAr = ["m","f","other"]
    let ageAr = ["<20","21-30","31-40","41-50","51-60",">60","other"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let iconSize = CGSize(width: 30, height: 30)
        mailIconIV.image = UIImage.fontAwesomeIcon(name: .envelope, style: .solid, textColor: .gray, size:iconSize)
        pwIconIV.image = UIImage.fontAwesomeIcon(name: .unlockAlt, style: .solid, textColor: .gray, size: iconSize)
        pw2IconIV.image = UIImage.fontAwesomeIcon(name: .unlockAlt, style: .solid, textColor: .gray, size: iconSize)
        phoneIconIV.image = UIImage.fontAwesomeIcon(name: .phone, style: .solid, textColor: .gray, size: iconSize)
        genderIconIV.image = UIImage.fontAwesomeIcon(name: .genderless, style: .solid, textColor: .gray, size: iconSize)
        incomeIconIV.image = UIImage.fontAwesomeIcon(name: .chartBar, style: .solid, textColor: .gray, size: iconSize)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerBtnClicked(_ sender: Any) {
        if pwTV.text != pw2TV.text {
            let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: NSLocalizedString("兩次輸入的密碼不符", comment: ""), type: .notification)
            errorAlert.show()
            return
        }
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: NSLocalizedString("載入中⋯⋯", comment: ""), type: NVActivityIndicatorType(rawValue: 6)!)
        Functions.registerApi(userName: userNameTV.text, email: emailTV.text, password: pwTV.text, phone: phoneTV.text, gender: genderAr[genderSC.selectedSegmentIndex], age: ageAr[ageSC.selectedSegmentIndex], complete: {(success:Bool, message:String) -> Void in
            if success{
                self.stopAnimating()
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message , type: .notification)
                let okAction = CDAlertViewAction(title: NSLocalizedString("OK", comment: ""), font: UIFont.systemFont(ofSize: 20) , textColor: .black, backgroundColor: .white, handler: {(action:CDAlertViewAction)-> Bool in
                    self.navigationController?.popViewController(animated: true)
                    return true
                })
                errorAlert.add(action: okAction)
                errorAlert.show()
                
            }else{
                self.stopAnimating()
                let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: message, type: .notification)
               
                errorAlert.show()
            }
            
        })
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
