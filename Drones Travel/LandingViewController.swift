//
//  LandingViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 27/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Spring

class LandingViewController: UIViewController {

    @IBOutlet var landing_1: SpringImageView!
    @IBOutlet var landing_2: SpringImageView!
    @IBOutlet var landing_3: SpringImageView!
    @IBOutlet var landing_4: SpringImageView!
    @IBOutlet var landing_5: SpringImageView!
    
    @IBOutlet var containerView: UIView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        landing_1.opacity = 0
        landing_2.opacity = 0
        landing_3.opacity = 0
        landing_4.opacity = 0
        landing_5.opacity = 0
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        animationStart()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func animationStart(){
        landing_5.animation = "fadeInDown"
        landing_5.duration = 0.5
        landing_5.animateNext {
            self.landing_2.animation = "fadeInDown"
            self.landing_2.duration = 3
            self.landing_2.animate()
        }
        landing_4.animation = "fadeInDown"
        landing_4.duration = 1
        landing_4.animateNext {
            self.landing_1.animation = "fadeInDown"
            self.landing_1.duration = 3
            self.landing_1.animate()
        }
        landing_3.animation = "fadeInDown"
        landing_3.duration = 3
        landing_3.animate()
        
        
        Functions.delayWithSeconds( 6, completion:{
            if Constants.defaults.get(for: Constants.accessTokenKey) == nil{
                self.goToLogin()
            }else if (Constants.countryAr.count) > 0{
                self.goToIndex()
            }else{
                self.showNetworkError()
                NotificationCenter.default.addObserver(self, selector: #selector(self.goToIndex), name: NSNotification.Name(rawValue: "countryArReady"), object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(self.showNetworkError), name: NSNotification.Name(rawValue: "countryArFetchFail"), object: nil)
            }
            
        })
    }

    func goToLogin()  {
        self.performSegue(withIdentifier: "goToLogin", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

        
    }
    
    @objc func goToIndex(){
        self.performSegue(withIdentifier: "goToIndex", sender: self)
    }
    
    @objc func showNetworkError(){
        let errorAlert = Functions.showNetworkErrorAlert {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getCountryApi()
        }
        errorAlert.show()
    }
}
