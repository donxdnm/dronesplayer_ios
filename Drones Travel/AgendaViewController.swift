//
//  AgendaViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 22/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import StepSlider
import SimpleImageViewer

class AgendaViewController: UIViewController{

    @IBOutlet weak var contentTV: UITableView!

    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var countryIV: UIImageView!
    @IBOutlet weak var TopBarV: UIView!
    
    @IBOutlet weak var sliderContainerview: UIView!
    @IBOutlet weak var shadowBarV: UIView!
    @IBOutlet weak var shadowLB: UILabel!

    
    var agenda :Agenda?
    var stepSlider: StepSlider = StepSlider.init(frame: CGRect.init(x: 0, y: UIScreen.main.bounds.height - 30, width: UIScreen.main.bounds.height - 300, height: 44))
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if agenda?.title != nil {
            titleLB.text = agenda?.title
            
            let countryName = (Constants.selectedCountryObj?.eng_name)!
            self.countryIV.image = UIImage.init(named: countryName)
        }
  
        self.shadowBarV.layer.masksToBounds = false
        self.shadowBarV.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.shadowBarV.layer.shadowColor = UIColor.black.cgColor
        self.shadowBarV.layer.shadowRadius = 2.0
        self.shadowBarV.layer.shadowOpacity = 0.4

        self.shadowLB.text = ""
        
        if (agenda?.agendaAr?.count)! > 1{
            
            self.setupStepSlider()
 
        }
        contentTV.rowHeight = UITableView.automaticDimension
        contentTV.estimatedRowHeight = 250
        contentTV.separatorStyle = .none
//        self.title = NSLocalizedString("申請流程", comment: "")
        let backButton = UIBarButtonItem()
        backButton.title =  NSLocalizedString("返回", comment: "") //in your case it will be empty or you can put the title of your choice
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        // Do any additional setup after loading the view.

        
        
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            tableBottomConstraint.constant = 70 + bottomPadding
        }
        if #available(iOS 11.0, *) {
            contentTV.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
        
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            stepSlider.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if agenda != nil{
            Functions.sendGATrack(viewName: agenda?.title)
        }
        // Recalculates height
        
        super.viewWillAppear(animated)
        if agenda?.title != nil{
            Functions.sendGATrack(viewName: agenda?.title)
        }
        contentTV.layoutIfNeeded()
        self.setUpTableViewFocus(tableview: contentTV)
        if (agenda?.agendaAr?.count)! > 1{
            setupStepSlider()
        }
        
        // Recalculates height
    }

    override func viewDidLayoutSubviews() {
        self.setupFooter()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupStepSlider(){
        
        stepSlider.translatesAutoresizingMaskIntoConstraints = false
        
        stepSlider.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        
        let uintCount = UInt(agenda?.agendaAr?.count as! Int)
        stepSlider.maxCount = uintCount
        stepSlider.tintColor = UIColor.orange
        stepSlider.trackColor = UIColor.black
        stepSlider.sliderCircleColor = UIColor.white
//        stepSlider.setTrackCircleImage(#imageLiteral(resourceName: "Gray_Circle"), for: .normal)
        self.stepSlider.layer.masksToBounds = false
        self.stepSlider.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.stepSlider.layer.shadowColor = UIColor.black.cgColor
        self.stepSlider.layer.shadowRadius = 10.0
        self.stepSlider.layer.shadowOpacity = 0.4
        stepSlider.addTarget(self, action: #selector(AgendaViewController.sliderValueDidChange(_:)), for: .valueChanged)
        
        self.view.addSubview(stepSlider)
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: stepSlider, attribute: .width , relatedBy: .equal, toItem: stepSlider.superview, attribute: .height, multiplier: 0.6, constant: 0.0))
        constraints.append(stepSlider.centerXAnchor.constraint(equalTo: sliderContainerview.centerXAnchor, constant: -20))
        constraints.append(stepSlider.centerYAnchor.constraint(equalTo: (contentTV?.centerYAnchor)!))

        NSLayoutConstraint.activate(constraints)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let configuration = ImageViewerConfiguration { config in
            config.imageView = tapGestureRecognizer.view as! UIImageView
        }
        
        present(ImageViewerController(configuration: configuration), animated: true)
//        let imageView = tapGestureRecognizer.view as! UIImageView
//        let scrollView = UIScrollView.init(frame: UIScreen.main.bounds)
//        let newImageView = UIImageView(image: imageView.image)
//        newImageView.frame = UIScreen.main.bounds
//        newImageView.backgroundColor = .black
//        newImageView.contentMode = .scaleAspectFit
//        newImageView.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
//        newImageView.addGestureRecognizer(tap)
//        self.view.addSubview(newImageView)
//        self.navigationController?.isNavigationBarHidden = true
//        self.tabBarController?.tabBar.isHidden = true
    }

}

extension AgendaViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let agendaCount = (self.agenda?.agendaAr?.count)!
        return agendaCount + 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            
            if indexPath.row < (agenda?.agendaAr?.count)! {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "StepContentCell") as! StepContentTableViewCell
                cell.stepNumLB.text = String(indexPath.row + 1)
                cell.stepNumLB.textColor = UIColor.black
                cell.stepNumLB.layer.cornerRadius = cell.stepNumLB.frame.width/2
                cell.stepNumLB.layer.masksToBounds = true
                cell.subjectLB.text = self.agenda?.agendaAr![indexPath.row].name
                cell.contentLB.text = self.agenda?.agendaAr![indexPath.row].content
                cell.setupRelatedUrl(url: (self.agenda?.agendaAr![indexPath.row].reference_link)!)
                let imageUrl = self.agenda?.agendaAr![indexPath.row].image
                if imageUrl != ""{
                    cell.setImgUrl(url: imageUrl!, tableView:tableView)
                    cell.attectedImgIV?.isHidden = false
                    cell.attectedImgHeight.isActive = false
                }else{
                    cell.setImgUrl(url: "", tableView:tableView)
                    cell.attectedImgIV?.isHidden = true
                    cell.attectedImgHeight.isActive = true
                }
                if indexPath.row == (self.agenda?.agendaAr?.count)! - 1{
                    cell.bottonIndicator.isHidden = true
                }
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.attectedImgIV?.isUserInteractionEnabled = true
                cell.attectedImgIV?.addGestureRecognizer(tapGestureRecognizer)
                
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "endingCell")
                return cell!
            }
     
        
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.isTracking || scrollView.isDecelerating){
            stepSlider.isUserInteractionEnabled = false
            var tableview: UITableView
            if let tableview = scrollView as? UITableView{
                if tableview == contentTV{
                    setUpTableViewFocus(tableview: tableview)
                }
            }
            stepSlider.isUserInteractionEnabled = true
            stepSlider.layoutIfNeeded()
        }
        
    }

    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func setupFooter(){
        contentTV.layoutIfNeeded()
        let screenHeight = UIScreen.main.bounds.height
        let tableHeight = screenHeight - 90
        var footerHeight = tableHeight - 44
        view.layoutIfNeeded()
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            let topPadding = view.safeAreaInsets.top
            footerHeight = footerHeight - bottomPadding - topPadding
        }
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: Int(contentTV.frame.width), height: Int(footerHeight)))
        customView.backgroundColor = UIColor.clear
        contentTV.tableFooterView = customView
        
    }
//    func setupFooter(){
//        contentTV.layoutIfNeeded()
//        let tableHeight = contentTV.frame.height
//        var footerHeight = tableHeight - 185
//        if #available(iOS 11.0, *) {
//            let bottomPadding = view.safeAreaInsets.bottom
//            footerHeight = footerHeight - bottomPadding
//        }
//        let customView = UIView(frame: CGRect(x: 0, y: 0, width: Int(contentTV.frame.width), height: Int(footerHeight)))
//        customView.backgroundColor = UIColor.clear
//        contentTV.tableFooterView = customView
//
//    }
    

    func setUpTableViewFocus(tableview: UITableView){
        var cellIndexThatFocus = 0
        var indexPath = IndexPath.init(row: 0, section: 0)
        if contentTV.visibleCells.count > 0{
            indexPath = tableview.indexPath(for: contentTV.visibleCells.first!)!
        }
        
        if (indexPath.row) < (agenda?.agendaAr?.count)! {
            let cellRect = tableview.rectForRow(at: indexPath )
            if tableview.bounds.contains(cellRect){
                cellIndexThatFocus = (indexPath.row)
            }else{
                cellIndexThatFocus = indexPath.row + 1
            }
            stepSlider.index = UInt(cellIndexThatFocus)
        }else{
            return
        }
        
        refreshVisiableStepLabelColor(cellIndexThatFocus: cellIndexThatFocus, listTable: tableview)
    }
    
    func refreshVisiableStepLabelColor(cellIndexThatFocus:Int, listTable tableview:UITableView){
        for cell in tableview.visibleCells{
            let visibleIndexPath = tableview.indexPath(for: cell)!
            if visibleIndexPath.row == (agenda?.agendaAr?.count)! {
                continue
            }else if visibleIndexPath.row == cellIndexThatFocus{
                (cell as! StepContentTableViewCell).stepNumLB.textColor = UIColor.white
                (cell as! StepContentTableViewCell).stepNumLB.backgroundColor = UIColor(red: 245/255, green: 119/255, blue: 50/255, alpha: 1.0)
            }else{
                (cell as! StepContentTableViewCell).stepNumLB.textColor = UIColor.white
                (cell as! StepContentTableViewCell).stepNumLB.backgroundColor = UIColor.black
            }
            
            
        }
        
    }
    
    @objc func sliderValueDidChange(_ sender:UISlider!){
        if !contentTV.isTracking && !contentTV.isDecelerating{
            let value = round(Double(stepSlider.index))
            contentTV.scrollToRow(at: IndexPath.init(row:Int(value) , section: 0) , at: .top, animated: false)
            refreshVisiableStepLabelColor(cellIndexThatFocus: Int(value), listTable: contentTV)
            print(Float(stepSlider.index))
        }
        
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    
}
