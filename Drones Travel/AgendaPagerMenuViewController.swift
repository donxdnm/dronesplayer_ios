//
//  AgendaPagerMenuViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 10/7/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Parchment
import NVActivityIndicatorView

class AgendaPagerMenuViewController: UIViewController {

    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var noDataLB: UILabel!
    @IBOutlet weak var singleAgendaContainerView: UIView!
    @IBOutlet weak var refreshBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (Constants.agendaAr.count) > 1 {
            stopNavIndication()
            noDataLB.isHidden = true
            singleAgendaContainerView.isHidden = true
            setupMenuView()
        }else if Constants.agendaAr.count == 1{
            stopNavIndication()
            noDataLB.isHidden = true
            singleAgendaContainerView.isHidden = false
            setupSingleAgendaView()
            
        }else{
            stopNavIndication()
            NotificationCenter.default.addObserver(self, selector: #selector(self.stopNavIndication), name: NSNotification.Name(rawValue: "agendaStepArReady"), object: nil)
            noDataLB.isHidden = false
            singleAgendaContainerView.isHidden = true
        }
        
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            tableBottomConstraint.constant = 70 + bottomPadding
        }

        // Do any additional setup after loading the view.
    }

    @IBAction func refreshBtnClicked(_ sender: Any) {
        IndexViewController.fetchAgendaFromApi(id: (Constants.selectedCountryObj?.id)!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupMenuView(){
        let pagingViewController = PagingViewController<PagingIndexItem>()
        addChild(pagingViewController)
        pagingViewController.view.isUserInteractionEnabled = true
        view.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pagingViewController.dataSource = self
        if #available(iOS 11.0, *) {
            let guide = self.view.safeAreaLayoutGuide
            pagingViewController.view.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        } else {
            NSLayoutConstraint(item: pagingViewController.view,
                               attribute: .top,
                               relatedBy: .equal,
                               toItem: view, attribute: .top,
                               multiplier: 1.0, constant: 0).isActive = true
        }
        NSLayoutConstraint.activate([
            pagingViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            
            ])
    }
    
    func setupSingleAgendaView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "agendaViewController") as! AgendaViewController
        viewController.agenda = Constants.agendaAr.first
        self.add(asChildViewController: viewController)
    }
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }

}

extension AgendaPagerMenuViewController:PagingViewControllerDataSource{
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T where T : PagingItem, T : Comparable, T : Hashable {
        let item = PagingIndexItem(index: index, title: Constants.agendaAr[index].menuTitle) as! T
        return item
    }
    
    
    func numberOfViewControllers<T>(in pagingViewController: PagingViewController<T>) -> Int {
        return Constants.agendaAr.count
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "agendaViewController") as! AgendaViewController
        viewController.agenda = Constants.agendaAr[index]
        
        return viewController
    }
    
    
}

extension AgendaPagerMenuViewController:NVActivityIndicatorViewable {
    
    @objc func stopNavIndication(){
        if Constants.agendaAr.count > 0{
            noDataLB.isHidden = true
            setupMenuView()
        }
        stopAnimating()
        
    }
}
