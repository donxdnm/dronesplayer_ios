//
//  AppDelegate.swift
//  Drones Travel
//
//  Created by Don Chu on 15/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Mapbox
import AlamofireObjectMapper
import Alamofire
import Siren
import CDAlertView
import IQKeyboardManager

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var errorAlert: CDAlertView?
    var hasNotification = false
    var notificationMessage :[String:String]? = nil
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        //change the app language for the pushnotification pulgin but not working anymore
//        UserDefaults.standard.set("tw", forKey: "app_lang")
//        UserDefaults.standard.set(["tw"], forKey: "AppleLanguages")
//        UserDefaults.standard.synchronize()
//        print(Locale.current.languageCode)
        //
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "Map_Screen_Bar_Top"), for: .default)
        
        UIApplication.shared.statusBarView?.backgroundColor = Constants.primaryColor
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.orange], for: .selected)

        getCountryApi()
        self.setupGoogleAnalytics()
        setupSiren()
        IQKeyboardManager.shared().isEnabled = true
        getCookeFormUserDefault()
        
        if UINavigationBar.conforms(to: UIAppearanceContainer.self){
            UINavigationBar.appearance().tintColor = UIColor.white
        }
        
        if application.responds(to: Selector.init("isRegisteredForRemoteNotifications")){
           
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }else{
            application.registerForRemoteNotifications(matching: [.alert, .badge, .sound])
        }

        return true
    }
    

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        print("token:" + deviceToken.base64EncodedString())
        Constants.deviceToken = deviceToken
        Functions.sendTokenToServer(url: Constants.domain + "/pnfw/register/", token: deviceToken, langCode: "tw", email: nil)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let id = userInfo["id"] as? String
        if let aps = userInfo["aps"] as? NSDictionary {
            
            if let alert = aps["alert"] as? String {
                hasNotification = true
                notificationMessage = ["title":alert,"id":id as! String]
                let state = UIApplication.shared.applicationState
                if state != .inactive{
                    handleNoticification()
                }
            }
            
        }
        
        
    }
    
    func handleNoticification(){
        if hasNotification && notificationMessage != nil{
            let alert = Functions.showPushNoticification(title: notificationMessage!["title"] as! String, handler: {
                UIApplication.topViewController()?.navigationController?.pushViewController(Functions.getPostDetailViewControler(post_id: self.notificationMessage!["id"] as! String), animated: true)
                self.hasNotification = false
                self.notificationMessage = nil
            })
    
            alert.show()
            
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        Siren.shared.checkVersion(checkType: .immediately)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        Siren.shared.checkVersion(checkType: .daily)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func getCountryApi(){
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters = ["action" : "get-app-map-country"]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{ (response: DataResponse<CountryResponse>) in
            
            let countryResponse = response.result.value
            print(countryResponse?.countryAr)
            switch response.result {
                case .success:
                    print("Validation Successful")
                    Constants.countryAr = (countryResponse?.countryAr)!
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "countryArReady"), object: nil)
                    break
                case .failure(let error):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "countryArFetchFail"), object: nil)
                    break
            }
            
        }

    }
    
    func setupGoogleAnalytics() {
        
        if
            let gai = GAI.sharedInstance(),
            let gaConfigValues = Bundle.main.infoDictionary?["GoogleAnalytics"] as? [String: String],
            let trackingId = gaConfigValues["TRACKING_ID"]
        {
            gai.logger.logLevel = .error
            gai.trackUncaughtExceptions = false
            gai.tracker(withTrackingId: trackingId)
            // gai.dispatchInterval = 120.0
        } else {
            assertionFailure("Google Analytics not configured correctly")
        }
    }

}

extension AppDelegate{
    func getCookeFormUserDefault(){
        if let cookieArray = UserDefaults.standard.array(forKey: "hz_tokens") {
            for cookieData in cookieArray {
                if let dict = cookieData as? [HTTPCookiePropertyKey : Any] {
                    if let cookie = HTTPCookie.init(properties : dict) {
                        HTTPCookieStorage.shared.setCookie(cookie)
                    }
                }
            }
        }
    }
}
extension AppDelegate: SirenDelegate{
    func setupSiren() {
        let siren = Siren.shared
        siren.countryCode = "HK"
        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 0
        // Optional
        siren.delegate = self as? SirenDelegate
        
        // Optional
        siren.debugEnabled = true
        
        // Optional - Change the name of your app. Useful if you have a long app name and want to display a shortened version in the update dialog (e.g., the UIAlertController).
        //        siren.appName = "Test App Name"
        // Optional - Change the various UIAlertController and UIAlertAction messaging. One or more values can be changes. If only a subset of values are changed, the defaults with which Siren comes with will be used.
        siren.alertMessaging = SirenAlertMessaging(updateTitle:
            NSAttributedString(string: "新更新！！", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]),
                                                    updateMessage: NSAttributedString(string: "請立即更新應用程式，以獲得更多功能", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]),
                                                    updateButtonMessage: NSAttributedString(string: "更新", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]),
                                                    nextTimeButtonMessage: NSAttributedString(string: "下次提醒", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]),
                                                    skipVersionButtonMessage: NSAttributedString(string: "跳過是次更新", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]))
        
        // Optional - Defaults to .Option
        //        siren.alertType = .option // or .force, .skip, .none
        // Optional - Can set differentiated Alerts for Major, Minor, Patch, and Revision Updates (Must be called AFTER siren.alertType, if you are using siren.alertType)
        siren.majorUpdateAlertType = .force
        siren.minorUpdateAlertType = .force
        siren.patchUpdateAlertType = .option
        siren.revisionUpdateAlertType = .option
        
        // Optional - Sets all messages to appear in Russian. Siren supports many other languages, not just English and Russian.
        //        siren.forceLanguageLocalization = .russian
        // Optional - Set this variable if your app is not available in the U.S. App Store. List of codes: https://developer.apple.com/library/content/documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/Chapters/AppStoreTerritories.html
        //        siren.countryCode = ""
        // Optional - Set this variable if you would only like to show an alert if your app has been available on the store for a few days.
        // This default value is set to 1 to avoid this issue: https://github.com/ArtSabintsev/Siren#words-of-caution
        // To show the update immediately after Apple has updated their JSON, set this value to 0. Not recommended due to aforementioned reason in https://github.com/ArtSabintsev/Siren#words-of-caution.
        //        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 3
        // Optional (Only do this if you don't call checkVersion in didBecomeActive)
        //        siren.checkVersion(checkType: .immediately)
    }
    
    func sirenDidShowUpdateDialog(alertType: Siren.AlertType) {
        print(#function, alertType)
    }
    
    func sirenUserDidCancel() {
        print(#function)
    }
    
    func sirenUserDidSkipVersion() {
        print(#function)
    }
    
    func sirenUserDidLaunchAppStore() {
        print(#function)
    }
    
    func sirenDidFailVersionCheck(error: Error) {
        print(#function, error)
    }
    
    func sirenLatestVersionInstalled() {
        print(#function, "Latest version of app is installed")
    }
    
    func sirenNetworkCallDidReturnWithNewVersionInformation(lookupModel: SirenLookupModel) {
        print(#function, "\(lookupModel)")
    }
    
    // This delegate method is only hit when alertType is initialized to .none
    func sirenDidDetectNewVersionWithoutAlert(title: String, message: String, updateType: UpdateType) {
        print(#function, "\n\(title)\n\(message).\nRelease type: \(updateType.rawValue.capitalized)")
    }
}

