//
//  ObjectHandler.swift
//  dronesplayer
//
//  Created by Don Chu on 30/5/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import ObjectMapper

class LawResponse: Mappable {
    var lawStepAr: [LawStep]?
    var title: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        lawStepAr <- map["data"]
        title <- map["title"]
    }
}

class LawStep: Mappable {
    var youtube_id: String?
    var image: String?
    var content: String?
    var reference_link :String?
    var name:String?

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        youtube_id <- map["youtube_id"]
        image <- (map["image"],JSONImageTransform())
        content <- map["content"]
        reference_link <- map["reference_link"]
        name <- map["name"]
        
    }
}
class CampaignResponse: Mappable {
    var result: String?
    var error: String?
    var data: [Campaign]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        error <- map["error"]
        data <- map["data"]
        
    }
}

class Campaign: Mappable {
    var campaign_url: String?
    var campaign_title: String?
    var campaign_cover: String?
    var campaign_icon :String?
    var campaign_subtitle:String?
    var ios_enable: Bool?
    var post_date: Date?
    var end_date: Date?

    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        campaign_url <- map["campaign_url"]
        campaign_title <- map["campaign_title"]
        campaign_cover <- map["campaign_cover"]
        campaign_icon <- map["campaign_icon"]
        campaign_subtitle <- map["campaign_subtitle"]
        ios_enable <- (map["ios_enable"], JSONTrueNFalseToBoolTransform())
        post_date <- (map["post_date"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
        end_date <- (map["end_date"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
        
    }
}

class RessetPwData: Mappable {
    var message: String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        
    }
}

class RessetPwResponse: Mappable {
    var result: String?
    var error: String?
    var data:RessetPwData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        error <- map["error"]
        data <- map["data"]
    }
}

class StaticPageContent: Mappable {
    var content: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        content <- map["content"]
    }
}

class LoginStatusCheckResponse: Mappable {
    var result: String?
    var error: String?
    var data: LoginStatusCheckData?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        error <- map["error"]
        data <- map["data"]
    }
}

class LoginStatusCheckData: Mappable {
    var message: String?
    var display_name: String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
        display_name <- map["display_name"]
        
    }
}

class AgendaResponse: Mappable {
    var agendas: [Agenda]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        agendas <- map["data"]
    }
}

class Agenda: Mappable {
    var agendaAr: [AgendaStep]?
    var menuTitle: String = ""
    var title: String = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        agendaAr <- map["data"]
        menuTitle <- map["short_name"]
        title <- map["name"]
    }
}

class AgendaStep: Mappable {
    var youtube_id: String?
    var image: String?
    var content: String?
    var reference_link :String?
    var name:String?
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        youtube_id <- map["youtube_id"]
        image <- (
            map["image"],JSONImageTransform())
        content <- map["content"]
        reference_link <- map["reference_link"]
        name <- map["name"]
        
    }
}

class ProfileResponse: Mappable {
    var result: String?
    var data : ProfileData?
    var error: String?

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        error <- map["error"]
        data <- map["data"]
    }
}

class ProfileData: Mappable {
    var display_name: String?
    var email : String?
    var mobile: String?
    var gender: String?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        display_name <- map["display_name"]
        email <- map["email"]
        mobile <- map["mobile"]
        gender <- map["gender"]
    }
}

class ChangePwResponse: Mappable {
    var result: String?
    var data : ChangePwData?
    var error: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        error <- map["error"]
        data <- map["data"]
    }
}

class ChangePwData: Mappable {
    var message: String?

    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
}

class LoginResponse: Mappable {
    var user_id: String?
    var email:String?
    var login_success : Bool?
    var error: String?
    var access_token: String?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        email <- map["email"]
        login_success <- (map["login_success"] ,JSONTrueNFalseToBoolTransform())
        error <- map["error"]
        access_token <- map["access_token"]
    }
}

class registerResponse: Mappable {
    var code: Int?
    var message : String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
    }
}

class LayerResponse: Mappable {
    var layerAr: [DroneLimitationLayer]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        layerAr <- map["data"]
    }
}

class DroneLimitationLayer: Mappable {
    var layer_id: String?
    var layer_name: String?
    var name: String?

    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        layer_id <- map["layer_id"]
        layer_name <- map["layer_name"]
        name <- map["name"]
        
    }
}
class IndexResponse: Mappable {
    var indexData: IndexData?
    
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        indexData <- map["data"]
    }
}
class IndexData: Mappable {
    var sliderData: SliderData?
    var ad:Ad?
    var newsData:IndexNewsData?
    var reviewData:IndexReviewData?
    var productData:IndexProductData?
    var recommendNews:IndexRecommendNewsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        sliderData <- map["slider"]
        ad <- map["ad_section"]
        newsData <- map["news_data"]
        reviewData <- map["review_data"]
        productData <- map["product_data"]
        recommendNews <- map["recommend_data"]
    }
}


class IndexProductData: Mappable {
    var products: [Product]?
    var display_order: Int?
    
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        products <- map["data"]
        display_order <- map["display_order"]
        
    }
}

class ProductResponse: Mappable {
    var data: [Product]?
    
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
}

class Product: Mappable {
    var id: String?
    var name: String?
    var cover: String?
    var link: String?
    var rating:String?
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        cover <- map["cover"]
        link <- map["link"]
        rating <- map["rating"]
        
    }
}

class SliderData: Mappable {
    var sliderAr: [Slider]?
    var display_order: Int?
    
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        sliderAr <- map["data"]
        display_order <- map["display_order"]
        
    }
}

class Slider: Mappable {
    var post_id: String?
    var post_type: String?
    var cover_img_url: String?
    var url: String?
    
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        post_id <- map["post_id"]
        post_type <- map["type"]
        cover_img_url <- map["cover"]
        url <- map["url"]
    }
}

class IndexNewsData: Mappable {
    var newsAr:[Cover]?
    var display_order:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        newsAr <- map["data"]
        display_order <- map["display_order"]
    }
}

class IndexRecommendNewsData: Mappable {
    var newsAr:[Cover]?
    var display_order:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        newsAr <- map["data"]
        display_order <- map["display_order"]
    }
}


class IndexReviewData: Mappable {
    var reviewAr:[Cover]?
    var display_order:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        reviewAr <- map["data"]
        display_order <- map["display_order"]
    }
}

class Cover: Mappable {
    var id:String?
    var cover:String?
    var title:String?
    var author:AuthorProfile?
    var date:Date?
    var tags:[Tag]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        cover <- map["cover"]
        title <- map["title"]
        author <- map["author"]
        date <- (map["date"], CustomDateFormatTransform(formatString: "yyyy-MM-dd"))
        tags <- map["tags"]
    }
}

class Tag: Mappable {
    var id:Int?
    var name:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        id <- map["term_id"]
    }
}

class AuthorProfile: Mappable {
    var name:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
    }
}
class Ad: Mappable {
    var content: String?
    var display_order: Int?
    
    
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        content <- map["data"]
        display_order <- map["display_order"]
        
    }
}

class CountryResponse: Mappable {
    var countryAr: [Country]?
    

    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        countryAr <- map["data"]
    }
}

class PostResponse : Mappable {
    var data: [Cover]?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class PostDetailResponse : Mappable {
    var data: Post?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class Post: Mappable {
    var tags: [Tag]?
    var id:Int?
    var cover:String?
    var title:String?
    var author: AuthorProfile?
    var link: String?
    var content:String?
    var date:Date?
    var related_post: [Cover]?
    var isReview :Bool?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        tags <- map["tags"]
        id <- map["id"]
        cover <- map["cover"]
        title <- map["title"]
        author <- map["author"]
        link <- map["link"]
        content <- map["content"]
        date <- (map["date"], CustomDateFormatTransform(formatString: "yyyy-MM-dd"))
        related_post <- map["related_post"]
        isReview <- (map["isReview"],JSONYAndNToBoolTransform())
    }
}

class DronesResponse: Mappable {
    var data: Drone?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class Drone: Mappable {
    var id: String?
    var images: [DroneImage]?
    var name: String?
    var features: [DroneFeature]?
    var link: String?
    var rating: String?
    var content: String?
    var specs_data: [DroneSpecs]?
    var related_post: [Cover]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        images <- map["images"]
        name <- map["name"]
        features <- map["features"]
        link <- map["link"]
        rating <- map["rating"]
        content <- map["content"]
        specs_data <- map["specs_data"]
        related_post <- map["related_post"]
    }
}

class DroneSpecs: Mappable {
    var name:String?
    var data:[DeoneSpecsInfo]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        data <- map["data"]
    }
}

class DeoneSpecsInfo: Mappable {
    var name:String?
    var value:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        value <- map["value"]
    }
}


class DroneImage: Mappable {
    var url: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        url <- map["url"]
    }
}

class DroneFeature: Mappable {
    var name: String?
    var icon: String?
    var value: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        icon <- map["icon"]
        value <- map["value"]
    }
}

class ProductSettingResponse: Mappable {
    var data: ProductSetting?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class ProductSetting: Mappable {
    var brands: [ProductSettingField]?
    var sortings: [ProductSettingField]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        brands <- map["brands"]
        sortings <- map["sorting"]
    }
}

class ProductSettingField: Mappable {
    var id: String?
    var name: String?
    
    required init?(map:Map){
        
        self.id <- map["id"]
        self.name <- map["name"]
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}

class PostCatResponse: Mappable {
    var catAr: [PostCat]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        catAr <- map["data"]
    }
}
class PostCat: Mappable {

    var name:String?
    var id:String?

    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
class Country: Mappable {
    var latitude: Double?
    var longitude: Double?
    var sealevel: Double?
    var zoomlevel :Double?
    var mapbox_map_id:String?
    var layer_id:String?
    var layer_name: String?
    var mapbox_style_url:String?
    var id:String?
    var name:String?
    var eng_name:String?
    var comingSoon:Bool?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        latitude <- (map["latitude"], JSONStringToDoubleTransform())
        longitude <- (map["longitude"], JSONStringToDoubleTransform())
        sealevel <- (map["sealevel"], JSONStringToDoubleTransform())
        zoomlevel <- (map["zoomlevel"], JSONStringToDoubleTransform())
        mapbox_map_id <- map["mapbox_map_id"]
        layer_id <- map["layer_id"]
        layer_name <- map["layer_name"]
        mapbox_style_url <- map["mapbox_style_url"]
        id <- map["id"]
        name <- map["name"]
        eng_name <- map["english_name"]
        comingSoon <- (map["coming_soon"],JSONYAndNToBoolTransform())
    }
}

class FavouriteNews:Codable{
    var news:[FavouriteNew] = [FavouriteNew]()
}

class FavouriteNew:Codable{
    var id:String = "0"
    var cover:String = ""
    var title:String = ""
    var author_name:String = ""
    var date:String? = "2011-01-01"
    
    init(id:String, cover:String, title:String, author_name:String, date:String) {
        self.id = id
        self.cover = cover
        self.title = title
        self.author_name = author_name
        self.date = date
    }
}
class JSONStringToIntTransform: TransformType {
    
    typealias Object = Int
    typealias JSON = String
    
    init() {}
    func transformFromJSON(_ value: Any?) -> Int? {
        if let strValue = value as? String {
            return Int(strValue)
        }
        return value as? Int ?? nil
    }

    
    func transformToJSON(_ value: Int?) -> String? {
        if let intValue = value {
            return "\(intValue)"
        }
        return nil
    }
}

class JSONStringToFloatTransform: TransformType {

    typealias Object = Float
    typealias JSON = String
    
    init() {}

    
    func transformFromJSON(_ value: Any?) -> Float? {
        if let strValue = value as? String {
            return Float(strValue)
        }
        return value as? Float ?? nil
    }
    
    func transformToJSON(_ value: Float?) -> String? {
        if let floatValue = value {
            return "\(floatValue)"
        }
        return nil
    }
}

class JSONStringToDoubleTransform: TransformType {
    
    typealias Object = Double
    typealias JSON = String
    
    init() {}
    
    
    func transformFromJSON(_ value: Any?) -> Double? {
        if let strValue = value as? String {
            return Double(strValue)
        }
        return value as? Double ?? nil
    }
    
    func transformToJSON(_ value: Double?) -> String? {
        if let doubleValue = value {
            return "\(doubleValue)"
        }
        return nil
    }
}

class JSONYAndNToBoolTransform: TransformType {
    
    typealias Object = Bool
    typealias JSON = String
    
    init() {}
    
    
    func transformFromJSON(_ value: Any?) -> Bool? {
        if let strValue = value as? String {
            if strValue == "y" {
                return true
            }else if strValue == "n" {
                return false
            }
        }
        return value as? Bool ?? nil
    }
    
    func transformToJSON(_ value: Bool?) -> String? {
        if let boolValue = value {
            return "\(boolValue)"
        }
        return nil
    }
}

class JSONTrueNFalseToBoolTransform: TransformType {
    
    typealias Object = Bool
    typealias JSON = String
    
    init() {}
    
    
    func transformFromJSON(_ value: Any?) -> Bool? {
        if let strValue = value as? String {
            if strValue == "true" {
                return true
            }else if strValue == "false" {
                return false
            }
        }
        return value as? Bool ?? nil
    }
    
    func transformToJSON(_ value: Bool?) -> String? {
        if let boolValue = value {
            return "\(boolValue)"
        }
        return nil
    }
}

class JSONImageTransform: TransformType {
    
    typealias Object = String
    typealias JSON = String
    
    init() {}
    
    
    func transformFromJSON(_ value: Any?) -> String? {
        if let strValue = value as? Bool {
            if strValue == false {
                return ""
            }
        }
        return value as? String ?? nil
    }
    
    func transformToJSON(_ value: String?) -> String? {
        if let strValue = value {
            return "\(strValue)"
        }
        return nil
    }
}
