//
//  LayoutPickerViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 19/7/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Mapbox

class LayerPickerViewController: UIViewController {
    
    var layers : [MGLStyleLayer] = []
    var layerInfo: [DroneLimitationLayer] = []
    @IBOutlet weak var layerBtn: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var layerTV: UITableView!
    @IBOutlet weak var layerContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        layerContainer.roundCorners([.topLeft, .topRight], radius: 20)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func layerBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backgroundViewClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LayerPickerViewController{
    func setupView(){
    }
    

}

extension LayerPickerViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return layers.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "layerCell", for: indexPath) as! LayerTableViewCell
        for layer in layers {
            if layer.identifier == layerInfo[indexPath.row].layer_id{
                cell.layerName.text = layerInfo[indexPath.row].layer_name
                cell.layerThatControl = layer
                if layer.isVisible{
                    cell.changeToggleOnIcon()
                }else{
                    cell.ChangeToggleOffIcon()
                }
            }
            
        }
        return cell
    }
    
    
    
}
