//
//  LayerTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 15/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Mapbox
class LayerTableViewCell: UITableViewCell {

    @IBOutlet weak var layerName: UILabel!
    @IBOutlet weak var toggleImage: UIImageView!
    var layerThatControl: MGLStyleLayer?
    var layerIsDisplaying = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        if layerIsDisplaying {
//            changeToggleOnIcon()
//        }else{
//            ChangeToggleOffIcon()
//        }
//        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            if !layerIsDisplaying{
                layerIsDisplaying = true
                changeToggleOnIcon()
            }else{
                layerIsDisplaying = false
                ChangeToggleOffIcon()
            }
        }

        // Configure the view for the selected state
    }

    func changeToggleOnIcon(){
        toggleImage.image = #imageLiteral(resourceName: "Tick")
        if layerThatControl != nil{
            layerThatControl?.isVisible = true
        }
        
    }
    
    func ChangeToggleOffIcon(){
        toggleImage.image = #imageLiteral(resourceName: "Gray_Circle")
        if layerThatControl != nil{
            layerThatControl?.isVisible = false
        }
    }
}
