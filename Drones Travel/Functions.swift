//
//  Functions.swift
//  dronesplayer
//
//  Created by Don Chu on 31/5/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import CDAlertView
import Alamofire
import PushNotifications

class Functions: NSObject {
    
    static func calculateEstimateImgHeightWithWidth(width: CGFloat,imgObject img:UIImage) -> CGFloat{
        let scale = img.size.width/img.size.height
        return width*scale
    }
    
    static func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    static func sendGATrack(viewName: String?){
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: viewName)

        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    static func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize.init(width: size.width * heightRatio, height:size.height * heightRatio)

        } else {
            newSize = CGSize.init(width: size.width * widthRatio, height:size.height * widthRatio)

        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height)

        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func showPushNoticification( title:String , handler: @escaping () -> ()) -> CDAlertView{
        let errorAlert =  CDAlertView(title: NSLocalizedString("新通知", comment: ""), message: title, type: .notification)
        let doneAction = CDAlertViewAction(title: "前往", handler: { action in
            handler()
            return true
        })
        errorAlert.add(action: doneAction)
        let cancelAction = CDAlertViewAction(title: "取消", handler: { action in
            return true
        })
        errorAlert.add(action: cancelAction)
        return errorAlert
    }
    
    static func showNetworkErrorAlert(handler: @escaping () -> ()) -> CDAlertView{
        let errorAlert =  CDAlertView(title: NSLocalizedString("提醒", comment: ""), message: "網絡出現問題，請稍後再試", type: .notification)
        let doneAction = CDAlertViewAction(title: "重試", handler: { action in
            handler()
            return true
        })
        errorAlert.add(action: doneAction)
        return errorAlert
    }

//
//    static func setupStoredConstant(){
//        if Defaults[.nightModeOn] == false {
//            Constants.primaryColor = UIColor.black
//            Constants.thirdColor = UIColor.white
//            Constants.secondColor = UIColor.gray
//
//        }else{
//            Constants.primaryColor = UIColor.white
//            Constants.thirdColor = UIColor.black
//            Constants.secondColor = UIColor.black
//        }
//
//        switch (Defaults[.storedFontSize]){
//            case "small":
//                Constants.fontSize = 15
//                Constants.titleFontSize = 24
//
//                break
//            case "medium":
//                Constants.fontSize = 18
//                Constants.titleFontSize = 32
//
//                break
//            case "large":
//                Constants.fontSize = 20
//                Constants.titleFontSize = 40
//
//                break
//
//            default:
//                break
//
//        }
//    }
    
    func setUpCircularBtn(btn: UIButton){
        btn.layer.borderWidth = 1
        btn.layer.masksToBounds = false
        btn.layer.borderColor = UIColor.black.cgColor
        btn.layer.cornerRadius = btn.frame.height/2
        btn.clipsToBounds = true
    }
    
    static func showAlert(currentViewController:UIViewController,messageBody body:String, messageTitle title:String){

        let alertViewController = UIAlertController(title: title, message: body ,preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { _ in
            // this code executes after you hit the OK button
            alertViewController.dismiss(animated: false, completion: nil)
        }
        alertViewController.addAction(action)
        
        currentViewController.present(alertViewController, animated: true, completion: nil)
    }
    
    static func convertSpecialCharacters(string: String) -> String {
        var newString = string
        let char_dictionary = [
            "&amp;" : "&",
            "&lt;" : "<",
            "&gt;" : ">",
            "&quot;" : "\"",
            "&apos;" : "'"
        ];
        for (escaped_char, unescaped_char) in char_dictionary {
            newString = newString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.literal, range: nil)
        }
        return newString
    }
    
    static func fetchProfileApi(complete:@escaping (Bool ,String, ProfileData?) -> Void){
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parameters: [String: String] = [
            "action" :"user-profile"
        ]
        let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject{ (response: DataResponse<ProfileResponse>) in
            switch response.result {
            case .success:
                let profileResponse = response.result.value
                print("Validation Successful")
                
                if profileResponse != nil{
                    if profileResponse?.result == "success"{
                        complete(true,"",profileResponse?.data)
                    }else{
                        complete(false,profileResponse?.error ?? "請稍後再試",nil)
                    }
                }

                break
            case .failure(let error):
                complete(false,NSLocalizedString("請稍後再試", comment: ""),nil)
                break
            }
        }
    }
    
    static func updateProfileApi(displayName:String, user_email:String, mobile:String,complete:@escaping (Bool ,String, ProfileData?) -> Void){
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        let parm = ["action":"user-profile-update","display_name" : displayName, "user_email" : user_email, "mobile" : mobile]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding:URLEncoding.httpBody).responseObject{ (response: DataResponse<ProfileResponse>) in
            switch response.result {
            case .success:
                let profileResponse = response.result.value
                print("Validation Successful")
                
                if profileResponse != nil{
                    if profileResponse?.result == "success"{
                        complete(true,"",profileResponse?.data)
                    }else{
                        complete(false,profileResponse?.error ?? "請稍後再試",nil)
                    }
                }
                
                break
            case .failure(let error):
                complete(false,NSLocalizedString("請稍後再試", comment: ""),nil)
                break
            }
        }
    }
    
    static func changePWapi(oldPW: String, newPW: String,complete:@escaping (Bool ,String) -> Void){
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        let parm = ["action":"user-change-pw","new_pw" : newPW, "old_pw" : oldPW]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{
            (response: DataResponse<ChangePwResponse>) in
            switch response.result {
            case .success:
                let changePwResponse = response.result.value
                print("Validation Successful")
                
                if changePwResponse != nil{
                    if changePwResponse?.result == "success"{
                        complete(true,(changePwResponse?.data?.message)!)
                    }else{
                        complete(false,(changePwResponse?.error)!)
                    }
                    
                }
                
                break
            case .failure(let error):
                complete(false, NSLocalizedString("請稍後再試", comment: ""))
                break
            }
        }
    }
    
    static func resetPwApi(userName: String?, complete:@escaping (Bool ,String) -> Void) {
        Constants.lawAr = []
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters? = nil
        if userName == nil {
            complete(false,NSLocalizedString("請填寫用戶名稱", comment: ""))
            return
        }else{
            parm  = ["action":"user-reset-pw","username" : userName!]
        }
        
        
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{
            (response: DataResponse<RessetPwResponse>) in
            switch response.result {
            case .success:
                let resetPwResponse = response.result.value
                print("Validation Successful")
                
                if resetPwResponse != nil{
                    if resetPwResponse?.result == "success"{
                        complete(true,resetPwResponse?.data?.message ?? "請稍後再試。")
                    }else{
                        complete(true,resetPwResponse?.error ?? "請稍後再試。")
                    }
                    
                    
                }
                
                break
            case .failure(let error):
                complete(false, NSLocalizedString("網絡異常，請稍後再試。", comment: ""))
                break
            }
        }
        
    }
    static func sendTokenToServer (url:String, token:Data, langCode:String, email:String? ){
        let deviceTokenString = token.reduce("") {
        return $0 + String(format: "%02x", $1)
        }
        var parameters: [String: String] = [
            "token" :deviceTokenString,
            "os"  : "iOS",
            "lang"  : langCode
        ]
        if email != nil{
            parameters["email"] = email
        }
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
    }
    static func checkLoginStatusApi(complete:@escaping (Bool,String?)-> Void) {
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters = ["action" : "check-login-status",
                                "login" : Constants.defaults.get(for: Constants.memberIdKey) ?? "",
                                "access_token" : Constants.defaults.get(for: Constants.accessTokenKey) ?? ""]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{ (response: DataResponse<LoginStatusCheckResponse>) in
            switch response.result {
            case .success:
                let loginStatusResponse = response.result.value
                print("Validation Successful")
                
                if loginStatusResponse != nil{
                    if loginStatusResponse?.result! == "success"{
                        
                       complete(true,loginStatusResponse?.data?.display_name)
                    }else{
                        complete(false,loginStatusResponse?.error)
                    }
                }else{
                    complete(false,"請重新登入")
                }
                
                break
            case .failure(let error):
                complete(false,"請重新登入")
            }
        }
    }
    static func loginApi(accessToken: String?, userName: String?, password: String?, useAccessToken:Bool , complete:@escaping (Bool ,String) -> Void) {
        Constants.lawAr = []
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters = ["login" : userName!,
                                "action":"user-login" ]
        if userName == nil {
            complete(false,NSLocalizedString("請填寫用戶名稱", comment: ""))
            return
        }
        
        if useAccessToken{
            if accessToken == nil || accessToken == ""{
                complete(false,NSLocalizedString("請再次登入", comment: ""))
            }else{
                parm["access_token"] = accessToken!
            }
        }else{
            if password == nil || password == ""{
                complete(false, NSLocalizedString("請填寫密碼", comment: ""))
            }else{
                parm["password"] = password!
            }
        }
        
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{
            (response: DataResponse<LoginResponse>) in
            switch response.result {
            case .success:
                let loginResponse = response.result.value
                print("Validation Successful")
                
                if loginResponse != nil{
                    if (loginResponse?.login_success)!{
                        
                        if(!useAccessToken){
                            if Constants.deviceToken != nil{
                                sendTokenToServer(url: Constants.domain + "/pnfw/register/", token: Constants.deviceToken!, langCode: "tw", email: loginResponse?.email)
                            }
                            
                         Constants.defaults.set((loginResponse?.access_token)!, for: Constants.accessTokenKey)
                            Constants.defaults.set((loginResponse?.user_id)!, for: Constants.memberIdKey)
                            
                            complete(true,NSLocalizedString("登入成功", comment: ""))
                        }else{
                            
                            complete(true,NSLocalizedString("請試多一次", comment: ""))
                        }
                        Functions.storeCookie(response: response)
                        
                    }else{
                        complete(false, (loginResponse?.error)!)
                        if useAccessToken{
                            Constants.defaults.clear(Constants.accessTokenKey)
                            Constants.defaults.clear(Constants.memberIdKey)
                        }
                    }
                    
                }
                
                break
            case .failure(let error):
                complete(false, NSLocalizedString("登入失敗", comment: ""))
                if useAccessToken{
                    Constants.defaults.clear(Constants.accessTokenKey)
                    Constants.defaults.clear(Constants.memberIdKey)
                }
                break
            }
        }
        
    }
    
    static func registerApi(userName: String?, email: String?, password: String?, phone:String?, gender:String?,age:String?, complete:@escaping (Bool ,String) -> Void){
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        if userName == "" || userName == nil {
            complete(false,NSLocalizedString("請填寫用戶名", comment: ""))
            return
        }
        if email == "" || email == nil {
            complete(false,NSLocalizedString("請填寫電郵地址", comment: ""))
            return
        }
        if password == "" || password == nil{
            complete(false,NSLocalizedString("請填寫密碼", comment: ""))
            return
        }
        if phone == nil{
            complete(false,NSLocalizedString("請填寫電話號碼", comment: ""))
            return
        }
        if gender == "" || gender == nil {
            complete(false,NSLocalizedString("請填寫性別", comment: ""))
            return
        }
        if age == "" || age == nil{
            complete(false,NSLocalizedString("請填寫你的年齡", comment: ""))
            return
        }
        let parm: Parameters = ["action":"user-register", "username" : userName!, "email" : email!, "password" : password!, "age" : age!, "gender" : gender!, "mobile" : phone!]
        
        
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{
            (response: DataResponse<registerResponse>) in
            switch response.result {
            case .success:
                let registerResponse = response.result.value
                print("Validation Successful")
                
                if registerResponse != nil{
                    if (registerResponse?.code)! == 200{
                        
                        
                        complete(true,(registerResponse?.message!)!)
                    }else{
                        complete(false, (registerResponse?.message!)!)
                    }
                    
                }
                
                break
            case .failure(let error):
                complete(false, NSLocalizedString("註冊失敗", comment: ""))
                break
            }
        }
    }
    
    static func fetchCampaign(complete:@escaping (Bool ,Campaign?,String) -> Void){
        let url = Constants.apiDomain + "api/campaign"
        
        Alamofire.request(url).responseObject { (response: DataResponse<CampaignResponse>) in
            switch response.result {
            case .success:
                let campaignResponse = response.result.value
                if (campaignResponse != nil && campaignResponse?.data?.count != 0){
                    
                    for campaign in (campaignResponse?.data)!{
                        if campaign.campaign_url != nil && ((campaign.campaign_url)! != "")  && campaign.ios_enable != nil && (campaign.ios_enable)!{
                            complete(true,campaign,"")
                            break
                        }
                    }
                    print("Validation Successful")
                    
                    
                }
               
                
                break
            case .failure(let error):
                complete(false,nil,NSLocalizedString("未能獲取最新活動，請稍後再試", comment: ""))
                break
            }
        }
    }
    
    static func accessFailCallback(actionBeforeMessageShow:@escaping (Bool) -> Void){
        
            Functions.loginApi(accessToken: Constants.defaults.get(for: Constants.accessTokenKey), userName: Constants.defaults.get(for: Constants.memberIdKey), password: nil, useAccessToken: true, complete: {(autoLoginSuccess:Bool,autoLoginMessage:String) in
                if autoLoginSuccess{
                    actionBeforeMessageShow(autoLoginSuccess)
                    let errorAlert =  CDAlertView(title: "", message: NSLocalizedString("請再試一次", comment: ""), type: .notification)
                    errorAlert.show()
            
                }else{
                    actionBeforeMessageShow(autoLoginSuccess)
                    let errorAlert =  CDAlertView(title: NSLocalizedString("", comment: ""), message: autoLoginMessage, type: .notification)
                    errorAlert.show()
                   
                }
            })
        
    }
    static func getPostDetailViewControler(post_id:String) -> DPNewsDetailViewController{
        let postDetailViewViewController:DPNewsDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "postDetailView") as! DPNewsDetailViewController
        postDetailViewViewController.setPostID(postID: post_id)
        return postDetailViewViewController
    }
    static func storeCookie(response:DataResponse<LoginResponse>){
        let cookies = HTTPCookieStorage.shared.cookies!
        var cookieArray = [ [HTTPCookiePropertyKey : Any ] ]()
        for cookie in cookies {
            cookieArray.append(cookie.properties!)
        }
        UserDefaults.standard.set(cookieArray, forKey: "hz_tokens")
    }
    
    static func clearCookie(){
        UserDefaults.standard.set(nil, forKey: "hz_tokens")
        let cstorage = HTTPCookieStorage.shared
    
        if let cookies = cstorage.cookies(for: URL.init(string: Constants.loginUrl)!) {
            for cookie in cookies {
                cstorage.deleteCookie(cookie)
            }
        }
    }
    
    static func matches(for regex: String!, in text: String!) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
//    static func showAlertForNewPostAPN(currentViewController: UIViewController, messageBody body:String, postDetailID postID: String){
//        
//        let title = NSLocalizedString("有新通知", comment: "")
//        let cancelButtonTitle = NSLocalizedString("取消", comment: "")
//        let okButtonTitle = NSLocalizedString("好", comment: "")
//        let alertViewController = UIAlertController(title: title, message: body ,preferredStyle: .alert)
//        
//        let deleteAction = UIAlertAction(title: cancelButtonTitle, style: .default) { (action) in
//            print("Deleted!")
//            alertViewController.dismiss(animated: true, completion: nil)
//        }
//        
//        let okAction = UIAlertAction(title: okButtonTitle, style: .default) { (action) in
//            if let homeViewController = currentViewController as? HomeViewController{
//                homeViewController.getSinglePostApi(postID: postID)
//            }else{
//                currentViewController.tabBarController?.selectedIndex = 0
//
//                for child in (currentViewController.tabBarController?.viewControllers)! {
//                    if child.restorationIdentifier == "postListNav" {
//                        let homeViewController = (child.childViewControllers[0]) as! HomeViewController
//                        //                    let postDetail = (self.storyboard?.instantiateViewControllerWithIdentifier("postDetail"))! as! PostDetailViewController
//                        
//                        homeViewController.directToPostById  = postID
//                        homeViewController.getSinglePostApi(postID: postID)
//                        
//                    }
//                }
//            }
//            
//            print("Ok!")
//            alertViewController.dismiss(animated: true, completion: nil)
//            
//        }
//        
//        alertViewController.addAction(deleteAction)
//        alertViewController.addAction(okAction)
//        
//        currentViewController.present(alertViewController, animated: true, completion: nil)
//
//    }
    

}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    func urlEncoded() -> String {
        let encodeUrlString = self.addingPercentEncoding(withAllowedCharacters:
            .urlQueryAllowed)
        return encodeUrlString ?? ""
    }
    
}

extension NSAttributedString {
    
    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            return nil
        }
        
        guard let attributedString = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString: attributedString)
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIView {
    func addDashedBorder() {
        let color = UIColor.red.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

extension UINavigationController {
    func setBackgroundImage(_ image: UIImage) {
        navigationBar.isTranslucent = true
        navigationBar.barStyle = .blackTranslucent
        
        let logoImageView = UIImageView(image: image)
        logoImageView.contentMode = .scaleAspectFill
        logoImageView.clipsToBounds = true
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        
        view.insertSubview(logoImageView, belowSubview: navigationBar)
        NSLayoutConstraint.activate([
            logoImageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            logoImageView.rightAnchor.constraint(equalTo: view.rightAnchor),
            logoImageView.topAnchor.constraint(equalTo: view.topAnchor),
            logoImageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor)
            ])
    }
}

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

extension UIButton {
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var action :(() -> Void)?
        }
        if action != nil {
            __.action = action
        } else {
            __.action?()
        }
    }
    
    @objc private func triggerActionHandleBlock() {
        self.actionHandleBlock()
    }
    
    func actionHandle(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: "triggerActionHandleBlock", for: control)
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

extension UITableViewCell {
    
    var superTableView: UITableView? {
        
        var view = superview
        
        while view != nil && !(view is UITableView) {
            view = view?.superview
        }
        
        return view as? UITableView
    }
}

extension UIButton {
    func leftImage(image: UIImage, renderMode: UIImage.RenderingMode) {
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width / 2)
        self.contentHorizontalAlignment = .left
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    func rightImage(image: UIImage, renderMode: UIImage.RenderingMode){
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left:image.size.width / 2, bottom: 0, right: 0)
        self.contentHorizontalAlignment = .right
        self.imageView?.contentMode = .scaleAspectFit
    }
}

extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }

    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
//
//    override open func draw(_ rect: CGRect) {
//        if let insets = padding {
//            self.drawText(in: rect.inset(by: insets))
//        } else {
//            self.drawText(in: rect)
//        }
//    }
//
//    override open var intrinsicContentSize: CGSize {
//        guard let text = self.text else { return super.intrinsicContentSize }
//
//        var contentSize = super.intrinsicContentSize
//        var textWidth: CGFloat = frame.size.width
//        var insetsHeight: CGFloat = 0.0
//        var insetsWidth: CGFloat = 0.0
//
//        if let insets = padding {
//            insetsWidth += insets.left + insets.right
//            insetsHeight += insets.top + insets.bottom
//            textWidth -= insetsWidth
//        }
//
//        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
//                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                        attributes: [NSAttributedString.Key.font: self.font], context: nil)
//
//        contentSize.height = ceil(newSize.size.height) + insetsHeight
//        contentSize.width = ceil(newSize.size.width) + insetsWidth
//
//        return contentSize
//    }
}

extension UITableViewCell{
    
    func removeCellSelectionColour(){
        let clearView = UIView()
        clearView.backgroundColor = UIColor.clear
        UITableViewCell.appearance().selectedBackgroundView = clearView
    }
    
}
//extension Bundle {
//    private static var bundle: Bundle!
//    
//    public static func localizedBundle() -> Bundle! {
//        if bundle == nil {
//            let appLang = UserDefaults.standard.string(forKey: "app_lang") ?? "ru"
//            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
//            bundle = Bundle(path: path!)
//        }
//        
//        return bundle;
//    }
//    
//    public static func setLanguage(lang: String) {
//        UserDefaults.standard.set(lang, forKey: "app_lang")
//        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
//        bundle = Bundle(path: path!)
//    }
//}


