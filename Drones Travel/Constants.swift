//
//  Constants.swift
//  Drones Travel
//
//  Created by Don Chu on 19/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//


import UIKit
import CDAlertView
import DefaultsKit

//struct Cookies: Codable {
//    let cookiesAr: [HTTPCookie]
//}
struct Constants {
    static var domain  = "https://dronesplayer.com/"
    static var apiDomain = "https://dronesplayer.com/"
//    static var apiDomain = "https://dronesplayer.com/"
    static var loginUrl = "https://dronesplayer.com/api/user_login"
    static var featureedPostCat :Int = 0
    static var reviewPostCat :Int = 0
    static var appVersion = ""
    static var headerColor = UIColor.black
    static var footerColor = UIColor.black
    static var textColor = UIColor.white
    static var primaryColor = UIColor.black
    static var secondColor = UIColor.gray
    static var thirdColor = UIColor.white
    static var selectedColor = UIColor.orange
    static var fontSize = 20
    static var titleFontSize = 30
    static var token = ""
    static let jsonDecoder = JSONDecoder()
    static var countryAr: [Country] = []
    static var selectedCountryObj :  Country?
    static var lawTitle: String = ""
    static var lawAr: [LawStep] = []
    static var agendaAr: [Agenda] = []
    static var layerAr: [DroneLimitationLayer] = []
    
    static var indexResponse: IndexResponse? = nil
    
    static var numberOfPostPerPage = 15;
    static var numberOfSlider = 5;
    
    static var deviceToken:Data?
    static var defaults = Defaults()
    static var memberIdKey = Key<String>("memberId")
    static var accessTokenKey = Key<String>("accessToken")
    static var favouritePostKey = Key<FavouriteNews>("favouritePost")
//    static var cookieToken = Key<Cookies>("cookieToken")
    enum IndexSectionType {
        case News
        case Review
    }

}




