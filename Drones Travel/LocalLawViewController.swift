//
//  LocalLawViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 22/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import StepSlider
class LocalLawViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var topBarV: UIView!
    @IBOutlet weak var countryIV: UIImageView!
    @IBOutlet weak var contentTV: UITableView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var noDataLB: UILabel!
    @IBOutlet weak var shadowLB: UILabel!
    @IBOutlet weak var shadowBarV: UIView!
    @IBOutlet weak var sliderContainerview: UIView!
    @IBOutlet weak var refreshBtn: UIButton!
    
    var stepSlider: StepSlider = StepSlider.init(frame: CGRect.init(x: 0, y: UIScreen.main.bounds.height - 30, width: UIScreen.main.bounds.height - 300, height: 44))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shadowBarV.layer.masksToBounds = false
        self.shadowBarV.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.shadowBarV.layer.shadowColor = UIColor.black.cgColor
        self.shadowBarV.layer.shadowRadius = 2.0
        self.shadowBarV.layer.shadowOpacity = 0.4
        self.shadowLB.text = ""
        contentTV.rowHeight = UITableView.automaticDimension
        contentTV.estimatedRowHeight = 250
        contentTV.separatorStyle = .none
        let countryName = (Constants.selectedCountryObj?.eng_name)!
        self.countryIV.image = UIImage.init(named: countryName)
        self.title = NSLocalizedString("地方法規", comment: "")
        let backButton = UIBarButtonItem()
        backButton.title =  NSLocalizedString("返回", comment: "") //in your case it will be empty or you can put the title of your choice
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        // Do any additional setup after loading the view.
        if (Constants.lawAr.count) > 0 {
            stopNavIndication()
            noDataLB.isHidden = true
            contentTV.isHidden = false
            refreshBtn.isHidden = true
        }else{
            stopNavIndication()
            NotificationCenter.default.addObserver(self, selector: #selector(self.stopNavIndication), name: NSNotification.Name(rawValue: "lawStepArReady"), object: nil)
            contentTV.isHidden = true
            noDataLB.isHidden = false
            refreshBtn.isHidden = false
        }
        if Constants.lawTitle != "" {
            titleLB.text = Constants.lawTitle
        }
        
        //addGesture()
        contentTV.sectionHeaderHeight = UITableView.automaticDimension;
        contentTV.estimatedSectionHeaderHeight = 22;
        contentTV.beginUpdates()
        contentTV.endUpdates()
        if ( UIDevice.current.model.range(of: "iPad") != nil){
            stepSlider.isHidden = true
        }
        
    }
    @IBAction func refreshBtnClicked(_ sender: Any) {
        IndexViewController.fetchLawFromApi(id: (Constants.selectedCountryObj?.id)!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Constants.lawTitle != nil{
            Functions.sendGATrack(viewName: Constants.lawTitle)
        }
        contentTV.layoutIfNeeded()
        self.setUpTableViewFocus(tableview: contentTV)
        if Constants.lawAr.count > 1{
            setupStepSlider()
        }
        
        // Recalculates height
        
    }
    
    override func viewDidLayoutSubviews() {
        self.setupFooter()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //    @objc func scrollUpAtContentTV(gesture: UISwipeGestureRecognizer) {
//        if gesture.direction == .up {
//            if stepSlider.index + 1 < Constants.lawAr.count{
//                contentTV.scrollToRow(at: IndexPath(row: Int(stepSlider.index + 1), section: 0), at: .top, animated: true)
//            }
//        }
//    }
//
//    @objc func scrollDownAtContentTV(gesture: UISwipeGestureRecognizer) {
//        if gesture.direction == .down{
//            if stepSlider.index - 1 > 0 {
//                contentTV.scrollToRow(at: IndexPath(row: Int(stepSlider.index - 1), section: 0), at: .top, animated: true)
//            }
//        }
//    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LocalLawViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.lawAr.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView.isEqual(contentTV) {
            if indexPath.row < Constants.lawAr.count{
                let cell = tableView.dequeueReusableCell(withIdentifier: "StepContentCell") as! StepContentTableViewCell
                cell.stepNumLB.text = String.fontAwesomeIcon(name: .circle)
                cell.stepNumLB.font = UIFont.fontAwesome(ofSize: 18, style: .solid)
                cell.stepNumLB.textColor = UIColor.black
                let contentText = Functions.convertSpecialCharacters(string: Constants.lawAr[indexPath.row].content!)
                cell.contentLB.text = contentText
                let relatedLink = Constants.lawAr[indexPath.row].reference_link!
                cell.setupRelatedUrl(url: relatedLink)
                if indexPath.row == Constants.agendaAr.count - 1{
                    if cell.bottonIndicator != nil{
                        cell.bottonIndicator.isHidden = true
                    }
                    
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "endingCell")
                return cell!
            }
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StepIndicatorCell")
            let indicatorLB = cell?.contentView.viewWithTag(1) as! UILabel
            indicatorLB.text = String.fontAwesomeIcon(name: .circle)
            indicatorLB.font = UIFont.fontAwesome(ofSize: 20, style: .solid)
            
            indicatorLB.textColor = UIColor.black
            return cell!
        }
        

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.isTracking || scrollView.isDecelerating){
            stepSlider.isUserInteractionEnabled = false
            var tableview: UITableView
            if let tableview = scrollView as? UITableView{
                if tableview == contentTV{
                    setUpTableViewFocus(tableview: tableview)
                }
            }
            stepSlider.isUserInteractionEnabled = true
            stepSlider.layoutIfNeeded()
        }
        
    }

    func setupFooter(){
        contentTV.layoutIfNeeded()
        let screenHeight = UIScreen.main.bounds.height
        let tableHeight = screenHeight - 90
        var footerHeight = tableHeight - 44
        view.layoutIfNeeded()
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            let topPadding = view.safeAreaInsets.top
            footerHeight = footerHeight - bottomPadding - topPadding
        }
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: Int(contentTV.frame.width), height: Int(footerHeight)))
        customView.backgroundColor = UIColor.clear
        contentTV.tableFooterView = customView
        
    }
    
    
    
    @objc func stopNavIndication(){
        
        contentTV.reloadData()
        if Constants.lawAr.count > 1{
            setupStepSlider()
        }
        setUpTableViewFocus(tableview: contentTV)
        stopAnimating()
        contentTV.isHidden = false
        noDataLB.isHidden = true
    }
    
    func setUpTableViewFocus(tableview: UITableView){
        var cellIndexThatFocus = 0
        let indexPath = tableview.indexPath(for: contentTV.visibleCells.first!)!
        if (indexPath.row) < Constants.lawAr.count {
            let cellRect = tableview.rectForRow(at: indexPath )
            let bounds = tableview.bounds
            if tableview.bounds.contains(cellRect){
                cellIndexThatFocus = (indexPath.row)
            }else{
                cellIndexThatFocus = indexPath.row + 1
            }
            stepSlider.index = UInt(cellIndexThatFocus)
        }else{
            return
        }

        refreshVisiableStepLabelColor(cellIndexThatFocus: cellIndexThatFocus, listTable: tableview)
    }
    
    func refreshVisiableStepLabelColor(cellIndexThatFocus:Int, listTable tableview:UITableView){
        for cell in tableview.visibleCells{
            let visibleIndexPath = tableview.indexPath(for: cell)!
            if visibleIndexPath.row == Constants.lawAr.count {
                continue
            }else if visibleIndexPath.row == cellIndexThatFocus{
                (cell as! StepContentTableViewCell).stepNumLB.textColor = UIColor(red: 245/255, green: 119/255, blue: 50/255, alpha: 1.0)
            }else{
                (cell as! StepContentTableViewCell).stepNumLB.textColor = UIColor.black
            }
            
            
        }
        
    }
    func setupStepSlider(){
        stepSlider.translatesAutoresizingMaskIntoConstraints = false
        
        stepSlider.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        //            stepSlider.transform = CGAffineTransform(scaleX: -1, y: -1)
        let uintCount = UInt(Constants.lawAr.count)
        stepSlider.maxCount = uintCount
        stepSlider.tintColor = UIColor.orange
        stepSlider.trackColor = UIColor.black
        stepSlider.sliderCircleColor = UIColor.white
        self.stepSlider.layer.masksToBounds = false
        self.stepSlider.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.stepSlider.layer.shadowColor = UIColor.black.cgColor
        self.stepSlider.layer.shadowRadius = 10.0
        self.stepSlider.layer.shadowOpacity = 0.4
        stepSlider.addTarget(self, action: #selector(AgendaViewController.sliderValueDidChange(_:)), for: .valueChanged)
        
        self.view.addSubview(stepSlider)
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: stepSlider, attribute: .width , relatedBy: .equal, toItem: stepSlider.superview, attribute: .height, multiplier: 0.6, constant: 0.0))
        constraints.append(stepSlider.centerXAnchor.constraint(equalTo: sliderContainerview.centerXAnchor, constant: -20))
        constraints.append(stepSlider.centerYAnchor.constraint(equalTo: (contentTV?.centerYAnchor)!))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc func sliderValueDidChange(_ sender:UISlider!){
        if !contentTV.isTracking && !contentTV.isDecelerating{
            let value = round(Double(stepSlider.index))
            contentTV.scrollToRow(at: IndexPath.init(row:Int(value) , section: 0) , at: .top, animated: false)
            refreshVisiableStepLabelColor(cellIndexThatFocus: Int(value), listTable: contentTV)
            print(Float(stepSlider.index))
        }

        
    }
    
    @objc func showNetworkError(){
        let errorAlert = Functions.showNetworkErrorAlert {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.getCountryApi()
        }
        errorAlert.show()
    }
}
