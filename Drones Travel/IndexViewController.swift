//
//  IndexViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 20/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Mapbox
import Spring
import Alamofire
import NVActivityIndicatorView
import SDWebImage
import CDAlertView
import NotificationBannerSwift
import SafariServices
import ESTabBarController_swift

class IndexViewController: UIViewController,MGLMapViewDelegate,NVActivityIndicatorViewable {

    @IBOutlet weak var rightMenuBtn: UIBarButtonItem!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var mapView:MGLMapView!
    @IBOutlet weak var countryPickerCV: InfiniteCollectionView!{
        didSet{
            countryPickerCV.infiniteDelegate = self
            countryPickerCV.infiniteDataSource = self
            refreshCountryPicker()
        }
    }
    @IBOutlet weak var markIV: UIImageView!
    @IBOutlet weak var detailTabBar: UITabBar!
    @IBOutlet weak var tabbarHeight: NSLayoutConstraint!
    @IBOutlet weak var drawerView: SpringView!
    @IBOutlet weak var drawerDetailTabBarView: SpringView!
    @IBOutlet var colorIndicationBottomDistence: NSLayoutConstraint!
    @IBOutlet var backBtn: UIBarButtonItem!
//    @IBOutlet var dpBtn: UIBarButtonItem!
    @IBOutlet var colorIndicationView: UIView!
    @IBOutlet weak var mainScreenLayer: UIImageView!
    

    
    var markAnimationRepeatTime : Float = 2
    var ourLayers: [MGLStyleLayer] = []
    var timer: Timer?
    var timerIsRunning = false
    var tabbarFrameHeight = 70
    var selectedCountry: String?
    var selectedCountryObj: Country?
    var countryAr : [Country] = Array()
    var GreenLayerExpression = ["Singapore","Hong Kong","Macau","Korea","China","Taiwan","Japan","Malaysia"]
    var yellowLayerExpression = [""]
    var redLayerExpression = ["Thailand","Vietnam"]
    var campaignBanner :NotificationBanner?
    var greenCountryLayerAr : [MGLStyleLayer] = []
    var redCountryLayerAr: [MGLStyleLayer] = []
    var yellowCountryLayerAr : [MGLStyleLayer] = []
    var countryName :[String] = []
    var isGoingToCampaign = false
    
    @IBOutlet weak var rowOneSwitch: UISwitch!
    @IBOutlet weak var rowTwoSwitch: UISwitch!
    @IBOutlet weak var rowThreeSwitch: UISwitch!
    @IBOutlet weak var dpIcon: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let imgView = UIImageView(image: #imageLiteral(resourceName: "OrangeBar"))
        imgView.contentMode = .scaleAspectFill
        imgView.frame = self.navigationController!.navigationBar.frame
//        self.navigationController?.navigationBar.addSubview(imgView)
        self.navigationController?.navigationBar.sendSubviewToBack(imgView)
        setupView()
        setSearchView()
        mapView.setCenter(CLLocationCoordinate2D(latitude: 25.913818, longitude: 116.363625), zoomLevel: 2, animated: false)
        
        // Do any additional setup after loading the view.
    }
    


    override func viewWillAppear(_ animated: Bool) {
        if (UIApplication.shared.delegate as! AppDelegate).hasNotification && (UIApplication.shared.delegate as! AppDelegate).notificationMessage != nil{
           
            (UIApplication.shared.delegate as! AppDelegate).hasNotification = false
            self.navigationController?.pushViewController(Functions.getPostDetailViewControler(post_id: (UIApplication.shared.delegate as! AppDelegate).notificationMessage!["id"] as! String), animated: true)
            
        }
        self.navigationItem.leftBarButtonItems?.removeAll()
//        self.navigationItem.leftBarButtonItems?.append(dpBtn)
        isGoingToCampaign = false
        if self.campaignBanner != nil{
            self.campaignBanner?.isHidden = false
            self.campaignBanner?.show()
        }else{
            fetchCampaign()
            
        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.campaignBanner?.dismiss()
        self.campaignBanner = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  "goToDetail" {
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: NSLocalizedString("載入中⋯⋯", comment: ""), type: NVActivityIndicatorType(rawValue: 6)!)
            if let destinationController = segue.destination as? CustomizedTabBarController{
                destinationController.setSelectedIndex(index: sender as! Int)
            }
        }
    }
    

    

    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        
        let layers = (mapView.style?.layers)!;
        
        
        for layerInfo in countryAr {
            for layer in layers {
                
                if (layer.identifier == layerInfo.eng_name){
                    ourLayers.append( layer)
                    if(GreenLayerExpression.contains(layerInfo.eng_name!)){
                        greenCountryLayerAr.append(layer)
                    }else if(redLayerExpression.contains(layerInfo.eng_name!)){
                        redCountryLayerAr.append(layer)
                    }else if(yellowLayerExpression.contains(layerInfo.eng_name!)){
                        yellowCountryLayerAr.append(layer)
                    }
//                    layer.isVisible = false
                }
            }
        }
    }
    @IBAction func rightMenuBtnClicked(_ sender: Any) {
        if !Constants.defaults.has(Constants.accessTokenKey){
            performSegue(withIdentifier: "indexGoToLogin", sender: nil)
        }else{
            performSegue(withIdentifier: "indexGoToProfile", sender: nil)
        }
    }
    
    @IBAction func redLayerToggleClicked(_ sender: Any) {
        toggleRedCountries()
    }
    
    @IBAction func yellowLayerToggleClicked(_ sender: Any) {
        toggleYellowCountries()
    }
    
    @IBAction func greenLayerToggleClicked(_ sender: Any) {
        toggleGreenCountries()
    }
    
    
    @IBAction func searchBtnClicked(_ sender: Any) {
        showSearchView()
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationItem.leftBarButtonItems?.removeAll()
//        self.navigationItem.leftBarButtonItems?.append(dpBtn)
        self.showCountryPicker()
    }
    @IBAction func dpBtnClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "goToDp", sender: nil)
        
    }

}

extension IndexViewController{
    func setupView(){
        
        self.title = NSLocalizedString("選擇要去的地方", comment: "")
        self.navigationController?.navigationBar.tintColor = UIColor.white
       
        rightMenuBtn.image = UIImage.fontAwesomeIcon(name: .userCircle, style: .solid , textColor: UIColor.lightGray, size: CGSize(width: 40, height: 40))
//        dpBtn.image = Functions.resizeImage(image: UIImage.init(named: "dp_Icon")!, targetSize: CGSize.init(width: 45, height: 45)).withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        searchBtn.titleLabel?.font = UIFont.fontAwesome(ofSize: 30)
        searchBtn.setTitle(String.fontAwesomeIcon(name: .search), for: .normal)
        for item in detailTabBar.items!{
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        detailTabBar.barTintColor = UIColor.clear

        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            tabbarHeight.constant += (window?.safeAreaInsets.bottom)!
        }
        detailTabBar.layoutIfNeeded()
        drawerDetailTabBarView.layer.opacity = 0
        
        rowOneSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        rowOneSwitch.backgroundColor = UIColor.black
        rowOneSwitch.layer.cornerRadius = 15
        rowTwoSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        rowTwoSwitch.backgroundColor = UIColor.black
        rowTwoSwitch.layer.cornerRadius = 15
        rowThreeSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        rowThreeSwitch.backgroundColor = UIColor.black
        rowThreeSwitch.layer.cornerRadius = 15
        
//        colorIndicationBottomDistence.isActive = false
        
        
    }
}

extension IndexViewController: InfiniteCollectionViewDataSource,InfiniteCollectionViewDelegate{
    func didSelectCellAtIndexPath(_ collectionView: UICollectionView, usableIndexPath: IndexPath) {
        if !countryAr[usableIndexPath.row].comingSoon!{
            self.selectCountry(index: usableIndexPath.row)
        }
    }
    
    func numberOfItems(_ collectionView: UICollectionView) -> Int
    {
        return countryAr.count
    }
    func cellForItemAtIndexPath(_ collectionView: UICollectionView, dequeueIndexPath: IndexPath, usableIndexPath: IndexPath)  -> UICollectionViewCell
    {
        let cell = countryPickerCV.dequeueReusableCell(withReuseIdentifier: "CountryCell", for: dequeueIndexPath) as! CountryPickerCollectionViewCell
        cell.countryIV.image = UIImage(named: countryAr[usableIndexPath.row].eng_name! )
        cell.countryLB.text = NSLocalizedString(countryAr[usableIndexPath.row].name! , comment: "")
        if countryAr[usableIndexPath.row].comingSoon!{
            cell.comingSoonIV.isHidden = false
        }else{
            cell.comingSoonIV.isHidden = true
        }
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    
//    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        if countryAr[indexPath.row].comingSoon!{
//            return false
//        }else{
//            return true
//        }
//    }
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !countryAr[indexPath.row].comingSoon!{
            self.selectCountry(index: indexPath.row)
        }
            
        

    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 70, height: 70)
    }
    


}

extension IndexViewController{
    func setupMarkAnimation(){
        self.markIV.animationImages = [ #imageLiteral(resourceName: "Located_Icon_White0001"),#imageLiteral(resourceName: "Located_Icon_White0002"),#imageLiteral(resourceName: "Located_Icon_White0003"),#imageLiteral(resourceName: "Located_Icon_White0004"),#imageLiteral(resourceName: "Located_Icon_White0005"),#imageLiteral(resourceName: "Located_Icon_White0006") ]
        self.markIV.animationDuration = 2
    }
    
    func hideAllLayer(){
        for layer in ourLayers {
            layer.isVisible = false
        }
    }
    
    func showCountryPicker(){
        
        self.markIV.stopAnimating()
        self.markIV.isHidden = true
        self.mapView.setCenter(CLLocationCoordinate2D(latitude: 25.913818, longitude: 116.363625), zoomLevel: 2, animated: false)
        for layer in ourLayers {
            layer.isVisible = true
        }
        countryPickerCV.isUserInteractionEnabled = true
//        colorIndicationBottomDistence.isActive = false
        self.drawerDetailTabBarView.animation = "fadeOut"
        self.drawerDetailTabBarView.duration = 1
        self.drawerDetailTabBarView.animate()
        self.drawerView.animation = "fadein"
        self.drawerView.duration = 1
        self.drawerView.animate()
        mapView.isUserInteractionEnabled = true
        self.searchBtn.isHidden = false
    }
    
    func showCountryDetailEntry(){
        
    }
}

extension IndexViewController:UITabBarDelegate{

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        self.showCountryPicker()
        
        self.performSegue(withIdentifier: "goToDetail", sender: tabBar.items?.index(of: item))
    }
}

extension IndexViewController{
    
    func setSearchView(){
        countryName = []
        
        for country in Constants.countryAr {
            countryName.append(country.eng_name!)
            countryName.append(country.name!)
        }
        
//        dataSource = SimplePrefixQueryDataSource(countryName)
        

    }
    
    func showSearchView(){
        colorIndicationView.isHidden = true
        drawerView.isHidden = true
        searchBtn.isHidden = true
    }
    
    @objc func hideSearchView(){
        self.setSearchView()
        self.colorIndicationView.isHidden = false
        self.drawerView.isHidden = false
        self.searchBtn.isHidden = false
    }
    
    func selectCountry(index:Int){
        
        countryPickerCV.layoutIfNeeded()
        countryPickerCV.isUserInteractionEnabled = false
        self.selectedCountryObj = countryAr[index]
        Functions.sendGATrack(viewName: self.selectedCountryObj?.name)
        Constants.selectedCountryObj = selectedCountryObj
        fetchCountryDetail()
        let indexpath = countryPickerCV.indexPathsForSelectedItems?.first
        countryPickerCV.scrollToItem(at: (indexpath)!, at: .centeredHorizontally, animated: true)
        countryPickerCV.layoutIfNeeded()
        ourLayers[index].isVisible = true
        mapView.isUserInteractionEnabled = false
        let countryCoordinate = CLLocationCoordinate2D.init(latitude: countryAr[index].latitude as! CLLocationDegrees, longitude: countryAr[index].longitude as! CLLocationDegrees)
        let sealevel:Double = countryAr[index].sealevel! 
        let camera = MGLMapCamera(lookingAtCenter: countryCoordinate, fromDistance: sealevel, pitch: 0, heading: 0 )
        mapView.fly(to: camera, completionHandler: {
            
            UIView.transition(with: self.markIV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.markIV.isHidden = false
            })
//            self.colorIndicationBottomDistence.isActive = true
            self.setupMarkAnimation()
            self.markIV.startAnimating()
            self.drawerView.animation = "fall"
            self.drawerView.duration = 1
            self.drawerView.animate()
            self.detailTabBar.selectedItem = nil
            self.drawerDetailTabBarView.animation = "fadeInUp"
            self.drawerDetailTabBarView.duration = 1
            self.drawerDetailTabBarView.animate()
            self.navigationItem.leftBarButtonItems?.removeAll()
            self.navigationItem.leftBarButtonItems?.append(self.backBtn)
            self.searchBtn.isHidden = true
        })
    }
}

extension IndexViewController{
    func setCountryAr(countryAr:[Country] ){
        self.countryAr = countryAr
        countryPickerCV.scrollToItem(at: IndexPath(item: 2, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @objc func refreshCountryPicker(){
        setCountryAr(countryAr: Constants.countryAr)
        countryPickerCV.reloadData()
        countryPickerCV.layoutIfNeeded()
    }
}

extension IndexViewController{
    func toggleGreenCountries(){
        for layer in greenCountryLayerAr {
            if layer.isVisible{
                layer.isVisible = false
                rowThreeSwitch.isOn = false
            }else{
                layer.isVisible = true
                rowThreeSwitch.isOn = true
            }
            
        }
    }
    
    func toggleYellowCountries(){
        for layer in yellowCountryLayerAr {
            if layer.isVisible{
                layer.isVisible = false
                rowTwoSwitch.isOn = false
            }else{
                layer.isVisible = true
                rowTwoSwitch.isOn = true
            }
            
        }
    }
    
    func toggleRedCountries(){
        for layer in redCountryLayerAr {
            if layer.isVisible{
                layer.isVisible = false
                rowOneSwitch.isOn = false
            }else{
                layer.isVisible = true
                rowOneSwitch.isOn = true
            }
            
        }
    }
}
extension IndexViewController{
    func fetchCountryDetail(){
        IndexViewController.fetchLawFromApi(id: (selectedCountryObj?.id!)!)
        IndexViewController.fetchAgendaFromApi(id: (selectedCountryObj?.id!)!)
        IndexViewController.fetchLayerFromApi(id: (selectedCountryObj?.id!)!)
    }
    
    class func fetchLawFromApi(id: String){
        Constants.lawAr = []
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters = ["action" : "get-app-map-country-law",
                                "id" : String(id)]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{ (response: DataResponse<LawResponse>) in
            switch response.result {
            case .success:
                let lawResponse = response.result.value
                print("Validation Successful")
                
                if lawResponse != nil{
                    if lawResponse?.lawStepAr != nil{
                        Constants.lawAr = (lawResponse?.lawStepAr)!
                    }
                    if lawResponse?.title != nil{
                        Constants.lawTitle = (lawResponse?.title)!
                    }
                    
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "lawStepArReady"), object: nil)
                break
            case .failure(let error):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "layerStepArFetchFail"), object: nil)
                break
            }
        }
    }
    
    class func fetchAgendaFromApi(id: String){
        Constants.agendaAr = []
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters = ["action" : "get-app-map-country-application",
                                "id" : String(id)]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{ (response: DataResponse<AgendaResponse>) in
            switch response.result {
            case .success:
                let agendaResponse = response.result.value
                if agendaResponse != nil{
                    if agendaResponse?.agendas != nil{
                        Constants.agendaAr = (agendaResponse?.agendas)!
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "agendaStepArReady"), object: nil)
                break
            case .failure(let error):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "agendaStepArFetchFail"), object: nil)
                break
            }
        }
    }
    
    class func fetchLayerFromApi(id: String){
        Constants.layerAr = []
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        var parm: Parameters = ["action" : "get-app-map-country-layer",
                                "id" : String(id)]
        let request = Alamofire.request(url, method: .post, parameters: parm, encoding: URLEncoding.httpBody).responseObject{ (response: DataResponse<LayerResponse>) in
            switch response.result {
            case .success:
                let layerResponse = response.result.value
                print("Validation Successful")
                if layerResponse != nil{
                    if layerResponse?.layerAr != nil{
                        Constants.layerAr = (layerResponse?.layerAr)!
                    }
                    
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "layerStepArReady"), object: nil)
                break
            case .failure(let error):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "layerStepArFetchFail"), object: nil)
                break
            }
            
        }
    }
    
    func fetchCampaign(){
        Functions.fetchCampaign(complete: {(success:Bool, campaign:Campaign?, error:String) -> Void in
            if success{
                let leftView = UIImageView.init()
                leftView.sd_setImage(with: URL(string:(campaign?.campaign_icon!)!))
                let rightHideBtn = UIButton.init()
                rightHideBtn.titleLabel?.text = ""
                
                rightHideBtn.setImage(UIImage.fontAwesomeIcon(name: .sortUp, style:.solid , textColor: .white, size: CGSize.init(width: 30, height: 30)), for: .normal)
                rightHideBtn.actionHandle(controlEvents: UIControl.Event.touchUpInside,
                                          ForAction:{() -> Void in
                                            self.campaignBanner?.isHidden = true
                })
                let banner = NotificationBanner(title: campaign?.campaign_title ?? "", subtitle: campaign?.campaign_subtitle ?? "", leftView: leftView, rightView:rightHideBtn, style: .warning)
                banner.autoDismiss = false
                banner.haptic = .none
                
                
                banner.onTap = {
                    self.startAnimating()
                    self.campaignBanner?.isHidden = true
                    if self.isGoingToCampaign{
                        return
                    }
                    if Constants.defaults.has(Constants.accessTokenKey) && Constants.defaults.get(for: Constants.accessTokenKey) != nil{
                        Functions.checkLoginStatusApi(complete: {(success:Bool,message:String?) in
                            self.stopAnimating()
                            if success{
                                let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
                                webViewViewController.url = campaign?.campaign_url
                                self.isGoingToCampaign = true
                            self.navigationController?.pushViewController(webViewViewController, animated: true)
                               

                            }else{
                                Functions.loginApi(accessToken: Constants.defaults.get(for: Constants.accessTokenKey), userName: Constants.defaults.get(for: Constants.memberIdKey), password: nil, useAccessToken: true, complete: {(loginSuccess:Bool,loginMessage:String) in
                                    if loginSuccess{
                                        let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
                                        webViewViewController.url = campaign?.campaign_url
                                        self.navigationController?.pushViewController(webViewViewController, animated: true)
                                      
                                    }else{
                                        let errorAlert =  CDAlertView(title:"", message: loginMessage, type: .notification)
                                        errorAlert.show()
                                    }
                                })
                            }
                        })
                        
                    }else{
                        self.performSegue(withIdentifier: "indexGoToLogin", sender: nil)
                    }
                        
                    
                }
                
                self.campaignBanner = banner
                self.campaignBanner?.show()
                
            }else{
                let errorAlert =  CDAlertView(title:"", message: error, type: .notification)
                errorAlert.show()
            }
        })
    }
    
}

