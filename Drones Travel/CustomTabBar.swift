//
//  CustomTabBar.swift
//  Drones Travel
//
//  Created by Don Chu on 23/7/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class CustomTabBar : UITabBar {
    @IBInspectable var height: CGFloat = 0.0
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        if height > 0.0 {
            
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                sizeThatFits.height = CGFloat(height) + (window?.safeAreaInsets.bottom)!
            }else{
                sizeThatFits.height = height
            }
        }
        return sizeThatFits
    }
}
