//
//  CountryPickerCollectionViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 20/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit


class CountryPickerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var countryIV: UIImageView!
    @IBOutlet weak var countryLB: UILabel!
    @IBOutlet weak var comingSoonIV: UIImageView!
    
}
