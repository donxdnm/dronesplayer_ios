//
//  DpNewsListViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 24/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Tabman
import Pageboy
import Alamofire
import NVActivityIndicatorView

class DpNewsListViewController: TabmanViewController,NVActivityIndicatorViewable  {

    var loading = false
    var catAr:[PostCat] = [PostCat]()
    var defaultReview = false
    var reviewCatIndex = 0
    var newsList:[NewsListTableViewController] = [NewsListTableViewController]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        newsList = [NewsListTableViewController]()
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: NSLocalizedString("載入中⋯⋯", comment: ""), type: NVActivityIndicatorType(rawValue: 6)!)
        setupView()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DpNewsListViewController: PageboyViewControllerDataSource,TMBarDataSource{

    
    
    
    func setupView(){
        
        fetchNewsCat()
        
    }
    
    func fetchNewsCat(){
        loading = true
        let parameters: [String: String] = [
            "action" : "get-post-category"
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<PostCatResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let postCatResponse = response.result.value as! PostCatResponse
                print("Validation Successful")
                
                if postCatResponse != nil && postCatResponse.catAr?.count ?? 0 > 0{
                    self.catAr = postCatResponse.catAr!
                    var index = 0
                    for cat in self.catAr{
                        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "newsList") as! NewsListTableViewController
                        viewController.catID = cat.id!
                        self.newsList.append(viewController)
                         // configure the bar
//                        if self.bars.isEmpty{
                            //self.BarItems.append(BarItem(title: cat.name ?? "unknow "))
//                        }else{
//                             self.bar.items?.append(Item(title: cat.name ?? "unknow "))
//                        }
                        if cat.name == "產品評測"{
                            self.reviewCatIndex = index
                        }
                        
                        index += 1
                    }
                    self.dataSource = self
                    // Create bar
                    let bar = TMBar.ButtonBar()
                    bar.backgroundColor = Constants.primaryColor
                    bar.indicator.tintColor = Constants.selectedColor
                    bar.backgroundView.style = .clear
                    bar.buttons.customize { (button) in
                        button.selectedTintColor = Constants.selectedColor
                        button.tintColor = Constants.thirdColor
                    }
                    
//                    bar.transitionStyle = .snap // Customize
                    
                    // Add to view
                    if self.bars.isEmpty{
                        self.addBar(bar, dataSource: self, at: .top)
                    }
                    
                    if self.defaultReview{
                        Functions.sendGATrack(viewName: "news_review_listing")
                        self.scrollToPage(.at(index: self.reviewCatIndex) , animated: true)
                    }
                
                }
                break
            case .failure(let error):
                print(error)
                let errorAlert = Functions.showNetworkErrorAlert {
                    self.fetchNewsCat()
                }
                errorAlert.show()
                break
            }
            self.stopAnimating()
            
        }
    }
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return catAr.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return newsList[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        
        return TMBarItem(title: catAr[index].name ?? "unknow")
    }
}
