//
//  DPNewsDetailViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 31/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import WebKit
import AMScrollingNavbar
import MediaBrowser

class DPNewsDetailViewController: UIViewController {
    @IBOutlet weak var headerImg: UIImageView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var containerSC: UIScrollView!
    @IBOutlet weak var tagLB: UILabel!
    @IBOutlet weak var authorLB: UILabel!
    @IBOutlet weak var dateLB: UILabel!
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var relatedPostTV: NestedInnerTableView!
    private var postID: String? = nil
    private var postDetail:Post? = nil
    private var contentWV:WKWebView? = nil
    private var observerWasAdded = false
    private var mediaArray = [String]()
    private var browser:MediaBrowser? = nil
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var relatedNewsTitle: UIView!
    
    
    func setPostID(postID: String) {
        self.postID = postID
    }


    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func setFavBtn(){
        var favPosts = FavouriteNews()
        if Constants.defaults.has(Constants.favouritePostKey) && Constants.defaults.get(for: Constants.favouritePostKey) != nil   {
            favPosts = Constants.defaults.get(for: Constants.favouritePostKey)!
        }else{
            Constants.defaults.set(favPosts, for: Constants.favouritePostKey)
        }
        
        var index = 0
        for favPost in (favPosts.news){
            if favPost.id == self.postID{
                (self.navigationItem.rightBarButtonItem?.customView as! UIButton).setImage(UIImage.fontAwesomeIcon(name: .heart, style: .solid, textColor: .red, size: CGSize.init(width: 30, height: 30)), for: .normal)
                return
            }
            
            index += 1
        }
        (self.navigationItem.rightBarButtonItem?.customView as! UIButton).setImage(UIImage.fontAwesomeIcon(name: .heart, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30)), for: .normal)
    }
    
    @objc func favBtnClicked(){
        print("fav")
        var favPosts = FavouriteNews()
        if Constants.defaults.has(Constants.favouritePostKey) && Constants.defaults.get(for: Constants.favouritePostKey) != nil   {
            favPosts = Constants.defaults.get(for: Constants.favouritePostKey)!
        }else{
             Constants.defaults.set(favPosts, for: Constants.favouritePostKey)
        }
        
        var index = 0
        for favPost in (favPosts.news){
            if favPost.id == self.postID{
                (self.navigationItem.rightBarButtonItem?.customView as! UIButton).setImage(UIImage.fontAwesomeIcon(name: .heart, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30)), for: .normal)
                favPosts.news.remove(at: index)
                return
            }
            
            index += 1
        }
        (self.navigationItem.rightBarButtonItem?.customView as! UIButton).setImage(UIImage.fontAwesomeIcon(name: .heart, style: .solid, textColor: .red, size: CGSize.init(width: 30, height: 30)), for: .normal)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let postDate = (postDetail?.date != nil) ? dateFormatter.string(from: self.postDetail!.date! ) : ""
        let favPost = FavouriteNew.init(id: self.postID!, cover: (self.postDetail?.cover)!, title: (self.postDetail?.title)!, author_name: (self.postDetail?.author?.name)!, date: postDate)
        favPosts.news.append(favPost)
        Constants.defaults.clear(Constants.favouritePostKey)
        Constants.defaults.set(favPosts, for: Constants.favouritePostKey)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchNews(postID: postID!)
        self.navigationController?.isNavigationBarHidden = false
        containerSC.delegate = self
        
        let faveButton = UIButton(type: .custom)
        faveButton.addTarget(self, action: #selector(favBtnClicked), for: UIControl.Event.touchUpInside)
        faveButton.frame = CGRect(x:0,y:0,width: 45, height:45)
        faveButton.setImage(UIImage.fontAwesomeIcon(name: .heart, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30)), for: .normal)
        let backBarButtonItem = UIBarButtonItem.init(customView: faveButton)
        self.navigationItem.rightBarButtonItem = backBarButtonItem;
        
        setFavBtn()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        do{
            contentWV?.scrollView.removeObserver(self, forKeyPath: "contentSize")
        }catch{
            //do nothing, obviously it wasn't attached because an exception was thrown
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupView(post:Post){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if post != nil{
            if post.title != nil{
                headerTitle.padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
                headerTitle.text = post.title!
                if post.isReview ?? false{
                    Functions.sendGATrack(viewName: "review - " + (postDetail?.title)!)
                }else{
                    Functions.sendGATrack(viewName: "news - " + (postDetail?.title)!)
                }
                
            }
            if post.cover != nil{
                let imgUrl = URL.init(string: post.cover!)
                headerImg.sd_setImage(with: imgUrl, placeholderImage: UIImage.init(named: "placeholder"))
            }
            if post.cover != nil{
                authorLB.text = post.author?.name
                authorLB.sizeToFit()
            }
            if post.date != nil{
                dateLB.text = dateFormatter.string(from: post.date!)
                dateLB.sizeToFit()
            }
            if post.tags != nil && post.tags?.count ?? 0 > 0 {
                let tags = post.tags
                var tagString = ""
                if ( (tags?.count)! > 0){
                    for tag in tags!{
                        let tagName = tag.name!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                        tagString += ","
                        tagString += tagName
                    }
                }
                
                tagLB.text = String(tagString.dropFirst(1))
            }
            if post.content != nil{
                let webView = WKWebView()
                //            webView.uiDelegate as! WKUIDelegate = self
                webView.translatesAutoresizingMaskIntoConstraints=false
                //            cell.containerV.translatesAutoresizingMaskIntoConstraints=false
                webViewContainer.addSubview(webView)
                webView.translatesAutoresizingMaskIntoConstraints = false
                webView.loadHTMLString(post.content!,baseURL: nil)
                webView.leadingAnchor.constraint(equalTo: webViewContainer.leadingAnchor, constant: 0).isActive = true
                
                webView.trailingAnchor.constraint(equalTo: webViewContainer.trailingAnchor, constant: 0).isActive = true
                webView.scrollView.isScrollEnabled = false
                webView.topAnchor.constraint(equalTo: webViewContainer.topAnchor, constant: 0).isActive = true
//                webView.uiDelegate = self
//                webView.navigationDelegate = self
                webView.scrollView.delegate = self
                webView.bottomAnchor.constraint(equalTo: webViewContainer.bottomAnchor, constant: 0).isActive = true
                contentWV = webView
                contentWV?.navigationDelegate = self
                webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
                observerWasAdded = true
                
            }
            if postDetail?.related_post?.count == 0{
                relatedNewsTitle.isHidden = true
            }else{
                relatedNewsTitle
                    .isHidden = false
            }
            relatedPostTV.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
            relatedPostTV.rowHeight = UITableView.automaticDimension;
            relatedPostTV.estimatedRowHeight = 120.0;
            relatedPostTV.dataSource = self
            relatedPostTV.delegate = self
            relatedPostTV.reloadData()
        }
        
    
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"{
            let contentHeight = contentWV?.scrollView.contentSize.height
            containerHeight.constant = contentHeight!
        }
    }
    func fetchNews(postID: String) {
        let parameters: [String: String] = [
            "action" : "post-details",
            "post_id" : postID
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<PostDetailResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let postDetailResponse = response.result.value as! PostDetailResponse
                
                print("Validation Successful")
                
                if postDetailResponse != nil && postDetailResponse.data != nil{
                    self.postDetail = postDetailResponse.data!
                    self.setupView(post: postDetailResponse.data!)
                    let htmlContentWithoutSrcset = postDetailResponse.data?.content?.replacingOccurrences(of: "(?i)srcset=\"[^\"]+\"", with: "", options: .regularExpression, range: nil)
                    
                    self.mediaArray = Functions.matches(for:  "(http[^\\s]+(jpg|jpeg|png)\\b)", in: htmlContentWithoutSrcset)
                }else{
                    print("error")
                }
                
                break
            case .failure(let error):
                let error = Functions.showNetworkErrorAlert {
                    self.fetchNews(postID: postID)
                }
                error.show()
                print(error)
                break
            }
        }
    }
    
    func fixTableViewInsets() {
        let zContentInsets = UIEdgeInsets.zero
        containerSC.contentInset = zContentInsets
        containerSC.scrollIndicatorInsets = zContentInsets
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        fixTableViewInsets()
    }
    
}


extension DPNewsDetailViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(Functions.getPostDetailViewControler(post_id: (postDetail?.related_post?[indexPath.row].id)!), animated: true)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postDetail?.related_post?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell") as! NewsTableViewCell
        newsCell.removeCellSelectionColour()
        let coverAr = postDetail?.related_post
        let imgUrl = URL.init(string: ((coverAr?[indexPath.row].cover)!))
        let tags = coverAr?[indexPath.row].tags
        var tagString = ""
        if ( (tags?.count)! > 0){
            for tag in tags!{
                let tagName = tag.name!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                tagString += " ・"
                tagString += tagName
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        newsCell.coverImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
        newsCell.titleLB.text = coverAr?[indexPath.row].title!
        newsCell.authorLB.text = coverAr?[indexPath.row].author?.name!
        newsCell.tagLB.text = tagString
        if  (coverAr?[indexPath.row].date) != nil {
            newsCell.dateLB.text = dateFormatter.string(from: (coverAr?[indexPath.row].date)!)
            newsCell.dateLB.sizeToFit()
        }
        return newsCell
    }
}

extension DPNewsDetailViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pan = scrollView.panGestureRecognizer
        let velocity = pan.velocity(in: scrollView).y
        if velocity < -5 {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
//            self.navigationController?.setToolbarHidden(true, animated: true)
        } else if velocity > 5 {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
//            self.navigationController?.setToolbarHidden(false, animated: true)
        }
    }
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

extension DPNewsDetailViewController:WKNavigationDelegate{
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            let imageExtensions = ["png", "jpg", "jpeg"]
           
            
        if(imageExtensions.contains(navigationAction.request.url?.pathExtension ?? "")) {
                browser = MediaBrowser(delegate: self)
                browser?.enableGrid = false
                browser?.enableSwipeToDismiss = false
                browser?.setCurrentIndex(at: mediaArray.lastIndex(of: (navigationAction.request.url?.absoluteString)!)!)
                self.navigationController?.pushViewController(browser!, animated: true)
            }else{
                let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
                webViewViewController.url = navigationAction.request.url?.absoluteString
                self.navigationController?.pushViewController(webViewViewController, animated: true)
            }
            
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
    
        print("no link")
        decisionHandler(WKNavigationActionPolicy.allow)
    }
}

extension DPNewsDetailViewController:MediaBrowserDelegate{
    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
        return mediaArray.count
    }

    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < mediaArray.count {
            return Media(url:  URL(string: mediaArray[index])!, caption: "")
            
        }

        return Media(image: UIImage.init(named: "placeholder")!, caption: "")
    }
    
    func mediaBrowserDidFinishModalPresentation(mediaBrowser: MediaBrowser){
        mediaBrowser.delegate = nil
        
    }
//    func thumbnail(for mediaBrowser: MediaBrowser, at index: Int) -> Media{
//        if index < mediaArray.count {
//            return Media(url:  URL(string: mediaArray[index])!, caption: "")
//
//        }
//
//        return Media(image: UIImage.init(named: "placeholder")!, caption: "")
//    }
}
