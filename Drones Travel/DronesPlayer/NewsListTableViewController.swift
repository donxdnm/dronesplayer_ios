//
//  NewsListTableViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 2/11/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Alamofire
import ESPullToRefresh

class NewsListTableViewController: UITableViewController {
    
    var catID = "0"
    var newsAr:[Cover] = [Cover]()
    var page = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 120.0;
        self.tableView.es.addPullToRefresh {
            [unowned self] in
            print("refreshing")
            self.tableView.es.stopPullToRefresh(ignoreDate: true)
            
            self.tableView.es.stopPullToRefresh(ignoreDate: true, ignoreFooter: false)
        }
        self.tableView.es.addInfiniteScrolling {
            [unowned self] in
            self.page += self.page
            self.fetchNews()
            
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        newsAr.removeAll()
        self.page == 1
        self.fetchNews()
    }
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: false)
        self.navigationController?.pushViewController(Functions.getPostDetailViewControler(post_id: newsAr[indexPath.row].id!), animated: true)
        
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsAr.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let imgUrl = URL.init(string: ((newsAr[indexPath.row].cover)!))
        let tags = newsAr[indexPath.row].tags
        var tagString = ""
        if ( (tags?.count)! > 0){
            for tag in tags!{
                let tagName = tag.name!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                tagString += " ・"
                tagString += tagName
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        guard  let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as? NewsTableViewCell  else {
            return UITableViewCell() }
        newsCell.coverImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
        newsCell.titleLB.text = newsAr[indexPath.row].title!
        newsCell.authorLB.text = newsAr[indexPath.row].author?.name!
        newsCell.tagLB.text = tagString
        if newsAr[indexPath.row].date != nil{
            newsCell.dateLB.text = dateFormatter.string(from: (newsAr[indexPath.row].date)!)
            newsCell.dateLB.sizeToFit()
        }
        

        return newsCell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func fetchNews(){
        let parameters: [String: String] = [
            "action" : "get-post-list",
            "page"  : String(page),
            "category"  : String(catID)
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<PostResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let postsResponse = response.result.value as! PostResponse
                print("Validation Successful")
                
                if postsResponse != nil && postsResponse.data?.count ?? 0 > 0{
                    self.newsAr = self.newsAr + postsResponse.data!
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    
                    
                }else{
                    
                }
                
                break
            case .failure(let error):
                print(error)
                break
            }
        }

    }
}
