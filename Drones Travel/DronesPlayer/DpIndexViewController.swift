//
//  DpIndexViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 22/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import WebKit
import SDWebImage
import FSPagerView
import UIKit
import ESPullToRefresh
import Alamofire
import ESTabBarController_swift
enum IndexSectionType {
    case News
    case Review
}
class DpIndexViewController: UIViewController{

    
    
    private var sliderAr : [Slider]? = nil
    private var newsAr : [Cover]? = nil
    private var reviewAr : [Cover]? = nil
    private var productAr: [Product]? = nil
    private var recommendNewsAr: [Cover]? = nil
    private var newsSectionTableAdpter : IndexDynamicSectionTableAdpter? = nil
    private var reviewSectionTableAdpter : IndexDynamicSectionTableAdpter? = nil
    private var recommendNewsSectionTableAdpter : IndexDynamicSectionTableAdpter? = nil
    private var page = 1
    private var loading = false
    private var keyword = ""
    @IBOutlet weak var indexTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBigTable()
        
        indexTable.es.addPullToRefresh {
            [unowned self] in
            print("refreshing")
            self.fetchIndexData()
            self.indexTable.es.stopPullToRefresh(ignoreDate: true)
            self.indexTable.es.stopPullToRefresh(ignoreDate: true, ignoreFooter: false)
        }
        indexTable.es.addInfiniteScrolling {
            [unowned self] in
            self.requestNews()
            
        }
        setupTableView() 
        // Do any additional setup after loading the view.
    }
    
    func setupBigTable(){
        sliderAr = Constants.indexResponse?.indexData?.sliderData?.sliderAr
        newsAr = Constants.indexResponse?.indexData?.newsData?.newsAr
        reviewAr = Constants.indexResponse?.indexData?.reviewData?.reviewAr
        recommendNewsAr = Constants.indexResponse?.indexData?.recommendNews?.newsAr
        newsSectionTableAdpter = IndexDynamicSectionTableAdpter.init(coverAr: newsAr!, type: IndexSectionType.News)
        newsSectionTableAdpter?.selectDelegate = self
        reviewSectionTableAdpter = IndexDynamicSectionTableAdpter(coverAr: reviewAr!, type: IndexSectionType.Review)
        reviewSectionTableAdpter?.selectDelegate = self
        recommendNewsSectionTableAdpter = IndexDynamicSectionTableAdpter.init(coverAr: recommendNewsAr!, type: IndexSectionType.News)
        recommendNewsSectionTableAdpter?.selectDelegate = self
        productAr = Constants.indexResponse?.indexData?.productData?.products
    }
    
    func setupSlider(sliderView:FSPagerView, sliderControl: FSPageControl, cell:SliderTableViewCell) {
        sliderView.dataSource = self
        sliderView.delegate = cell
        cell.selectDelegate = self
         cell.sliderAr = sliderAr
        sliderControl.numberOfPages = sliderAr?.count ?? 0
        sliderControl.contentHorizontalAlignment = .left
        sliderControl.setFillColor(.gray, for: .normal)
        sliderControl.setFillColor(Constants.selectedColor, for: .selected)
        
    }
    
    func setupTableView(){
        self.indexTable.rowHeight = UITableView.automaticDimension;
        self.indexTable.estimatedRowHeight = 15.0;
        
    }
    
    func fixTableViewInsets() {
        let zContentInsets = UIEdgeInsets.zero
        indexTable.contentInset = zContentInsets
        indexTable.scrollIndicatorInsets = zContentInsets
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        fixTableViewInsets()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Functions.sendGATrack(viewName: "index")
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSearchResult"{
            let viewController = segue.destination as! NewsSearchViewController
            viewController.keyword = keyword
        }else if segue.identifier == "indexGoToProductDetail"{
            let viewController = segue.destination as! DPProductDetailViewController
            viewController.setPostID(postID: sender as! String)
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

    @IBAction func goToNewsPage(_ sender: Any) {
        if let tabView = self.navigationController?.viewControllers.first as? ESTabBarController{
            (tabView as! ESTabBarController).selectedIndex = 1
        }
    }
    @IBAction func goToProductPage(_ sender: Any) {
        if let tabView = self.navigationController?.viewControllers.first as? ESTabBarController{
            (tabView as! ESTabBarController).selectedIndex = 3
        }
    }
    @IBAction func goToReviewPage(_ sender: Any) {
        if let tabView = self.navigationController?.viewControllers.first as? ESTabBarController{
            
            (tabView as! ESTabBarController).selectedIndex = 1
            if let viewController = tabView.selectedViewController as? DpNewsListViewController{
                viewController.defaultReview = true
            }
        }
    }
}

extension DpIndexViewController: FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return sliderAr?.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let imgUrl = URL.init(string: (sliderAr?[index].cover_img_url)!)
        cell.imageView?.sd_setImage(with: imgUrl)
        
        return cell
    }

    
}



extension DpIndexViewController:UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == indexTable {
            if(indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "searchBarCell")
                return cell!
            }else if (indexPath.row == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell") as! SliderTableViewCell
               
                setupSlider(sliderView: cell.sliderView, sliderControl: cell.sliderControl, cell: cell)
                return cell
            }else if (indexPath.row == 2){
                let cell = tableView.dequeueReusableCell(withIdentifier: "iconSectionCell") as! IconSectionTableViewCell
                cell.removeCellSelectionColour()
                cell.selectDelegate = self
                return cell
            }else{
                // indexPath - 1 because api don't have iconsection yet
                let indexData = (Constants.indexResponse?.indexData)!
                return orderingSection(tableView ,displayOrder: indexPath.row - 1,data: indexData)
            }
            
           
        }else{
            let cell: UITableViewCell? = nil
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == indexTable{
            return 8
            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func orderingSection(_ tableView: UITableView,displayOrder:Int,data:IndexData)->UITableViewCell{
        
        let cell: UITableViewCell? = nil
        if data.ad?.display_order == displayOrder {
            let cell = tableView.dequeueReusableCell(withIdentifier: "adSectionCell") as! AdSectionTableViewCell
            cell.removeCellSelectionColour()
            let webView = WKWebView()
            //            webView.uiDelegate as! WKUIDelegate = self
            webView.translatesAutoresizingMaskIntoConstraints=false
            webView.navigationDelegate = self
            //            cell.containerV.translatesAutoresizingMaskIntoConstraints=false
            cell.containerV.addSubview(webView)
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.loadHTMLString((data.ad?.content!)!,baseURL: nil)
            webView.leadingAnchor.constraint(equalTo: cell.containerV.leadingAnchor, constant: 0).isActive = true
            
            webView.trailingAnchor.constraint(equalTo: cell.containerV.trailingAnchor, constant: 0).isActive = true
            webView.scrollView.isScrollEnabled = false
            webView.topAnchor.constraint(equalTo: cell.containerV.topAnchor, constant: 0).isActive = true
            
            webView.bottomAnchor.constraint(equalTo: cell.containerV.bottomAnchor, constant: 0).isActive = true
           
            return cell
        }else if data.newsData?.display_order == displayOrder {
            let cell = tableView.dequeueReusableCell(withIdentifier:"newsSectionCell") as! NewsSectionTableViewCell
            cell.removeCellSelectionColour()
            cell.indexNewsTable.dataSource = newsSectionTableAdpter as! UITableViewDataSource
            cell.indexNewsTable.delegate = newsSectionTableAdpter as! UITableViewDelegate
            cell.layoutSubviews()
            return cell

        }else if  data.reviewData?.display_order == displayOrder{
            let cell = tableView.dequeueReusableCell(withIdentifier:"reviewSectionCell") as! ReviewSectionTableViewCell
            cell.removeCellSelectionColour()
            cell.indexReviewTable.dataSource = reviewSectionTableAdpter as! UITableViewDataSource
            cell.indexReviewTable.delegate = reviewSectionTableAdpter as! UITableViewDelegate
            cell.layoutSubviews()
            return cell
            
            
        }else if data.productData?.display_order == displayOrder{
            let thiscell = tableView.dequeueReusableCell(withIdentifier: "productSectionCell") as! ProductSectionTableViewCell
            thiscell.removeCellSelectionColour()
            let imgUrl = URL.init(string: ((productAr?.first?.cover)!))
            thiscell.productImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
            thiscell.productLB.text = productAr?.first?.name
            
            thiscell.goToProductDetailBtn.addTarget(self, action: #selector(DpIndexViewController.cellGoToProductDetail(sender:)), for: .touchUpInside)
            (thiscell.goToProductDetailBtn as! PassableUIButton).id =  (productAr?.first?.id)!
            
            return thiscell
        }else if data.recommendNews?.display_order == displayOrder{
            let cell = tableView.dequeueReusableCell(withIdentifier:"recommendNewsSectionCell") as! RecommendNewsTableViewCell
            cell.removeCellSelectionColour()
            cell.newsTable.dataSource = recommendNewsSectionTableAdpter as! UITableViewDataSource
            cell.newsTable.delegate = recommendNewsSectionTableAdpter as! UITableViewDelegate
            cell.layoutSubviews()
            return cell
        }
        return cell!
    }
    

}

class IndexDynamicSectionTableAdpter: NSObject,UITableViewDataSource,UITableViewDelegate {
   
    
    
    var coverAr: [Cover]? = nil
    var type: IndexSectionType = .News
    var selectDelegate:MyCustomCellDelegator!
    
    init(coverAr : [Cover] , type:IndexSectionType) {
        self.coverAr = coverAr
        self.type = type
    }
    
    func updateCover(coverAr:[Cover]){
        self.coverAr = coverAr
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectDelegate.callSegueFromCell( id: (coverAr?[indexPath.row].id)!)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = nil
        var imgUrl:URL? = nil
        if coverAr?[indexPath.row].cover != nil{
            imgUrl = URL.init(string: ((coverAr?[indexPath.row].cover)!))
        }
        let tags = coverAr?[indexPath.row].tags
        var tagString = ""
        if ( (tags?.count)! > 0){
            for tag in tags!{
                let tagName = tag.name!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                tagString += " #"
                tagString += tagName
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if type == .News{
            guard  let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as? NewsTableViewCell  else {
                return UITableViewCell() }
            newsCell.coverImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
            newsCell.titleLB.text = coverAr?[indexPath.row].title!
            newsCell.authorLB.text = coverAr?[indexPath.row].author?.name!
            newsCell.tagLB.text = tagString
             newsCell.dateLB.text = dateFormatter.string(from: (coverAr?[indexPath.row].date)!)
            newsCell.dateLB.sizeToFit()
            newsCell.dateLB.textAlignment = .center
            cell = newsCell
        }else if type == .Review{
            guard let reviewCell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as? ReviewTableViewCell  else {
                return UITableViewCell() }
            reviewCell.coverImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
            reviewCell.titleLB.text = coverAr?[indexPath.row].title!
            reviewCell.authorLB.text = coverAr?[indexPath.row].author?.name!
            reviewCell.tagLB.text = tagString
            reviewCell.dateLB.text = dateFormatter.string(from: (coverAr?[indexPath.row].date)!)
            reviewCell.dateLB.sizeToFit()
            cell = reviewCell
        }

       
       
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coverAr?.count ?? 0
    }
    

}

extension DpIndexViewController :MyCustomCellDelegator{
    func callSegueFromCell(id post_id: String) {
        self.navigationController?.pushViewController(Functions.getPostDetailViewControler(post_id: post_id), animated: true)
    }
    
    func callSegueFromCell(url feature_url: String) {
        let webView = WebViewViewController()
        webView.url = feature_url
        self.navigationController?.pushViewController(webView, animated: true)
    }
    
   @IBAction  func cellGoToProductDetail(sender: PassableUIButton){
    print(sender.id)
        self.performSegue(withIdentifier: "indexGoToProductDetail", sender: sender.id)
    }
    
    func toWebViewfromCell(name pageUrl: String) {
        let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
        webViewViewController.url = Constants.domain + pageUrl
        
        self.navigationController?.pushViewController(webViewViewController, animated: true)
    }
    
    func requestNews(){
        page += 1
        loading = true
        let parameters: [String: String] = [
            "action" : "get-recommend-post-list",
            "page" : String(page)
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<PostResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let moreNewsResponse = response.result.value as! PostResponse
                print("Validation Successful")
                
                if moreNewsResponse != nil && moreNewsResponse.data?.count ?? 0 > 0{
                    self.recommendNewsAr! += moreNewsResponse.data!
                    (self.recommendNewsSectionTableAdpter as! IndexDynamicSectionTableAdpter).updateCover(coverAr: self.recommendNewsAr!)
                    if let cell = self.indexTable.cellForRow(at: IndexPath.init(row: self.indexTable.numberOfRows(inSection: 0) - 1, section: 0)) as? RecommendNewsTableViewCell{
                        cell.newsTable.reloadData()
                        cell.layoutSubviews()
                    }
                    self.indexTable.beginUpdates()
                    self.indexTable.endUpdates()
                    self.indexTable.es.stopLoadingMore()
                }else{
                    self.indexTable.es.stopLoadingMore()
                    self.indexTable.es.noticeNoMoreData()
                }
                
                break
            case .failure(let error):
                print(error)
                self.page -= 1
                let errorAlert = Functions.showNetworkErrorAlert {
                    self.requestNews()
                }
                errorAlert.show()
                self.indexTable.es.stopLoadingMore()
                self.indexTable.es.noticeNoMoreData()
                break
            }
        }
        
    }
    
    func fetchIndexData(){
        
        let parameters: [String: String] = [
            "action" : "get-index-section"
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<IndexResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let indexResponse = response.result.value
                print("Validation Successful")
                
                if indexResponse != nil{
                    Constants.indexResponse = indexResponse
                    self.setupBigTable()
                    self.indexTable.reloadData()
                }else{
                    Constants.indexResponse = nil
                }
                
                break
            case .failure(let error):
                print(error)
                Constants.indexResponse = nil
                let errorAlert = Functions.showNetworkErrorAlert {
                    self.fetchIndexData()
                }
                errorAlert.show()
                break
            }
        }
    }
}

extension DpIndexViewController:UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        keyword = searchBar.text ?? ""
        self.performSegue(withIdentifier: "goToSearchResult", sender: nil)
        view.endEditing(true)
        
    }

    
}

extension DpIndexViewController:WKNavigationDelegate{
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            print("link")
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            let webView = WebViewViewController()
            webView.url = navigationAction.request.url?.absoluteString
            self.navigationController?.pushViewController(webView, animated: true)
            return
        }
        print("no link")
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
}
protocol MyCustomCellDelegator {
    func callSegueFromCell(id post_id: String)
    func callSegueFromCell(url feature_url: String)
    func toWebViewfromCell(name pageUrl: String)
}

class PassableUIButton: UIButton{
    var id: String = ""

}

public extension ES where Base: UIScrollView {
    public func addPullToRefresh(handler: @escaping ESRefreshHandler) -> ESRefreshHeaderView {
        removeRefreshHeader()
        let header = ESRefreshHeaderView(frame: CGRect.zero, handler: handler)
        (header.animator as! ESRefreshHeaderAnimator).pullToRefreshDescription =
        NSLocalizedString("拉下以更新", comment: "")
        (header.animator as! ESRefreshHeaderAnimator).releaseToRefreshDescription =
            NSLocalizedString("放鬆即可更新", comment: "")
        let headerH = header.animator.executeIncremental
        header.frame = CGRect.init(x: 0.0, y: -headerH /* - contentInset.top */, width: self.base.bounds.size.width, height: headerH)
        self.base.addSubview(header)
        self.base.header = header
        return header
        
    }
    
    public func addInfiniteScrolling(handler: @escaping ESRefreshHandler) -> ESRefreshFooterView {
        removeRefreshFooter()
        let footer = ESRefreshFooterView(frame: CGRect.zero, handler: handler)
        (footer.animator as! ESRefreshFooterAnimator
        ).loadingDescription = NSLocalizedString("載入中", comment: "")
        (footer.animator as! ESRefreshFooterAnimator
            ).noMoreDataDescription = NSLocalizedString("沒有更多內容了", comment: "")
        (footer.animator as! ESRefreshFooterAnimator
            ).loadingMoreDescription = NSLocalizedString("載入更多內容", comment: "")
        let footerH = footer.animator.executeIncremental
        footer.frame = CGRect.init(x: 0.0, y: self.base.contentSize.height + self.base.contentInset.bottom, width: self.base.bounds.size.width, height: footerH)
        self.base.addSubview(footer)
        self.base.footer = footer
        return footer
    }
}
