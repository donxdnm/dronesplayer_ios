//
//  SettingViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 24/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import FontAwesome_swift
import NVActivityIndicatorView
class SettingViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var favImg: UIImageView!
    @IBOutlet weak var clearFavImg: UIImageView!
    @IBOutlet weak var aboutUsImg: UIImageView!
    @IBOutlet weak var privacyImg: UIImageView!
    @IBOutlet weak var serviceImg: UIImageView!
    @IBOutlet weak var versionLB: UILabel!
    @IBOutlet weak var profileSection: UIView!
    @IBOutlet weak var loginSection: UIView!
    @IBOutlet weak var userDisplayName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        Functions.sendGATrack(viewName: "setting")
        let size = CGSize(width: 30, height: 30)
        userDisplayName.text = NSLocalizedString("載入中⋯⋯", comment: "")
        startAnimating(size, message: NSLocalizedString("載入中⋯⋯", comment: ""), type: NVActivityIndicatorType(rawValue: 6)!)
        if Constants.defaults.has(Constants.accessTokenKey) && Constants.defaults.get(for: Constants.accessTokenKey) != "" && Constants.defaults.get(for: Constants.accessTokenKey) != nil{
            Functions.checkLoginStatusApi(complete: {(success:Bool,message:String?) in
                
                if success{
                    self.profileSection.isHidden = false
                    self.loginSection.isHidden = true
                    self.userDisplayName.text = message
                    self.stopAnimating()
                    
                }else{
                    self.stopAnimating()
                    self.profileSection.isHidden = true
                    self.loginSection.isHidden = false
                }
            })
        }else{
            self.profileSection.isHidden = true
            self.loginSection.isHidden = false
            self.stopAnimating()
        }
    }
    
    func setupView(){
        favImg.image = UIImage.fontAwesomeIcon(name: .heart, style: .solid, textColor: Constants.selectedColor, size: CGSize.init(width: 30, height: 30))
        clearFavImg.image = UIImage.fontAwesomeIcon(name: .trash, style: .solid, textColor: Constants.selectedColor, size: CGSize.init(width: 30, height: 30))
        privacyImg.image = UIImage.fontAwesomeIcon(name: .lock, style: .solid, textColor: Constants.selectedColor, size: CGSize.init(width: 30, height: 30))
        serviceImg.image = UIImage.fontAwesomeIcon(name: .file, style: .solid, textColor: Constants.selectedColor, size: CGSize.init(width: 30, height: 30))
        versionLB.text = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
    @IBAction func loginBtnClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "settingToLogin", sender: nil)
    }
    
    @IBAction func profileBtnClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "settingGoToProfile", sender: nil)
    }
    
    @IBAction func aboutUsBtnClicked(_ sender: Any) {
        let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
        webViewViewController.staticPageType = .aboutUS
        
        self.navigationController?.pushViewController(webViewViewController, animated: true)
    }
    @IBAction func clearFavPostBtnClicked(_ sender: Any) {
        Constants.defaults.clear(Constants.favouritePostKey)
        Functions.showAlert(currentViewController: self, messageBody: "成功消除記錄", messageTitle: "提醒")
    }
    @IBAction func privacyBtnClicked(_ sender: Any) {
        let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
        webViewViewController.staticPageType = .privacy
        
        self.navigationController?.pushViewController(webViewViewController, animated: true)
    }
    @IBAction func serviceBtnClicked(_ sender: Any) {
        let webViewViewController:WebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webView") as! WebViewViewController
        webViewViewController.staticPageType = .service
        
        self.navigationController?.pushViewController(webViewViewController, animated: true)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "settingToLogin"{
            let viewController = segue.destination as! MemberViewController
            viewController.segueIDAfterLogin = MemberViewController.segue.toSetting
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
 

}
