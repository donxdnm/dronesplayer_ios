//
//  DpProductListViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 24/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import FontAwesome_swift
import ESPullToRefresh
import NVActivityIndicatorView
import ObjectMapper

class DpProductListViewController: UIViewController {

    @IBOutlet weak var loading: NVActivityIndicatorView!
    @IBOutlet weak var brandSelectBtn: UIButton!
    @IBOutlet weak var sortingSelectBtn: UIButton!
    @IBOutlet weak var productCV: UICollectionView!
    @IBOutlet weak var brandSettingMenuTV: UITableView!
    @IBOutlet weak var sortingSettingMenuTV: UITableView!
    
    var productAr:[Product] = [Product]()
    var brandsSetting:[ProductSettingField]? = nil
    var sortingSetting:[ProductSettingField]? = nil

    var selectedBrand = ProductSettingField.init(map: Map.init(mappingType: .fromJSON, JSON:["id":"all","name":"全部"]))
    var selectedSorting = ProductSettingField.init(map: Map.init(mappingType: .fromJSON, JSON:["id":"date-d","name": "日期 (新 > 舊)"]))
    var page = 1
    override func viewDidLoad() {
        super.viewDidLoad()
//        brandSelectBtn.leftImage(image: UIImage.fontAwesomeIcon(name: .caretDown, textColor: .lightGray, size: CGSize.init(width: 30, height: 30)), renderMode: .alwaysOriginal)
//
//        sortingSelectBtn.leftImage(image: UIImage.fontAwesomeIcon(name: .caretDown, textColor: .lightGray, size: CGSize.init(width: 30, height: 30)), renderMode: .alwaysOriginal)
        self.productCV.es.addPullToRefresh {
            [unowned self] in
            print("refreshing")
            self.productCV.es.stopPullToRefresh(ignoreDate: true)
            self.page == 1
            self.fetchProducts()
            self.productCV.es.stopPullToRefresh(ignoreDate: true, ignoreFooter: false)
        }
        self.productCV.es.addInfiniteScrolling {
            [unowned self] in
            self.page += 1
            self.fetchProducts()
            
        }
        setupTopButton()
        brandSettingMenuTV.delegate = self
        brandSettingMenuTV.dataSource = self
        sortingSettingMenuTV.delegate = self
        sortingSettingMenuTV.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        page = 1
        productAr.removeAll()
        productCV.reloadData()
        fetchProducts()
        fetchSetting()
        brandSettingMenuTV.isHidden = true
        sortingSettingMenuTV.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Functions.sendGATrack(viewName: "drones")
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! DPProductDetailViewController
        viewController.setPostID(postID: sender as! String)
    }
 
    @IBAction func brandSelectBtnClicked(_ sender: Any) {
        brandSettingMenuTV.reloadData()
        UIView.transition(with: brandSettingMenuTV, duration: 0.4, options: .transitionCrossDissolve, animations: { self.brandSettingMenuTV.isHidden = false}, completion: nil)
       
    }
    @IBAction func braandSelectCancelBtnClicked(_ sender: Any) {
        UIView.transition(with: brandSettingMenuTV, duration: 0.4, options: .transitionCrossDissolve, animations: { self.brandSettingMenuTV.isHidden = true}, completion: nil)
    }
    @IBAction func sortingSelectBtnClicked(_ sender: Any) {
        sortingSettingMenuTV.reloadData()
        UIView.transition(with: sortingSettingMenuTV, duration: 0.4, options: .transitionCrossDissolve, animations: { self.sortingSettingMenuTV.isHidden = false}, completion: nil)
    }
    
    @IBAction func sortingSelectCancelBtnClicked(_ sender: Any) {
        UIView.transition(with: sortingSettingMenuTV, duration: 0.4, options: .transitionCrossDissolve, animations: { self.sortingSettingMenuTV.isHidden = true}, completion: nil)
    }
}

extension DpProductListViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "goToProductDetail", sender: productAr[indexPath.row].id)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productAr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath as IndexPath) as! ProductCollectionViewCell
        let imgUrl = URL.init(string: (productAr[indexPath.row].cover)!)
        cell.productImg.sd_setImage(with: imgUrl)
        cell.productLB.text = productAr[indexPath.row].name
        if productAr[indexPath.row].rating != nil {
            cell.rateLB.text = productAr[indexPath.row].rating
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2
        return CGSize(width: itemSize, height: itemSize*1.5)
    }
    
}

extension DpProductListViewController{
    func fetchProducts(){
        if page == 1{
           productAr.removeAll()
            productCV.reloadData()
            loading.startAnimating()
        }
        
        let parameters: [String: String] = [
            "action" : "get-drones-info",
            "page"  : String(page),
            "band"  :selectedBrand?.id ?? "all",
            "sort"  : selectedSorting?.id ?? "date-d"
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<ProductResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let postsResponse = response.result.value as! ProductResponse
                print("Validation Successful")
                
                if postsResponse != nil && postsResponse.data?.count ?? 0 > 0{
                    self.productAr = self.productAr + postsResponse.data!
                    self.productCV.dataSource = self
                    self.productCV.reloadData()
                    
                    
                }else{
                    
                }
                
                break
            case .failure(let error):
                print(error)
                let errorAlert = Functions.showNetworkErrorAlert {
                    self.fetchProducts()
                }
                errorAlert.show()
                break
            }
            if self.page == 1{
                self.loading.stopAnimating()
            }
            
        }
        
    }
    
    func fetchSetting(){
        loading.startAnimating()
        let parameters: [String: String] = [
            "action" : "get-drones-page-setting"
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<ProductSettingResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let postsResponse = response.result.value as! ProductSettingResponse
                print("Validation Successful")
                
                if postsResponse != nil && postsResponse.data != nil {
                    self.brandsSetting = postsResponse.data?.brands
                    self.sortingSetting = postsResponse.data?.sortings
                    self.brandSettingMenuTV.reloadData()
                    
                }else{
                    
                }
                
                break
            case .failure(let error):
                print(error)
                let errorAlert = Functions.showNetworkErrorAlert {
                    self.fetchSetting()
                }
                errorAlert.show()
                break
            }
            self.loading.stopAnimating()
        }
    }
    
    func setupTopButton(){
        brandSelectBtn.setTitle( "▼ " +  (selectedBrand?.name)!, for: .normal)
        sortingSelectBtn.setTitle("▼ "  + (selectedSorting?.name)!, for: .normal)
        brandSettingMenuTV.isHidden = true
        sortingSettingMenuTV.isHidden = true
    }
}

extension DpProductListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedBrand?.id != brandsSetting?[indexPath.row].id || selectedSorting?.id != sortingSetting?[indexPath.row].id{
            if tableView == brandSettingMenuTV{
                selectedBrand = brandsSetting?[indexPath.row]
            }else if tableView == sortingSettingMenuTV{
                selectedSorting = sortingSetting?[indexPath.row]
            }
            page = 1
            fetchProducts()
            setupTopButton()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == brandSettingMenuTV {
            return brandsSetting?.count ?? 0
        }else if tableView == sortingSettingMenuTV{
            return sortingSetting?.count ?? 0
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == brandSettingMenuTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "brandCell") as! UITableViewCell
            (cell.viewWithTag(1) as! UILabel).text = brandsSetting?[indexPath.row].name
            return cell
        }else if tableView == sortingSettingMenuTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "sortingCell") as! UITableViewCell
            (cell.viewWithTag(1) as! UILabel).text = sortingSetting?[indexPath.row].name
            return cell
        }
        return UITableViewCell()
    }
    
}
