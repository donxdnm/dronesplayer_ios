//
//  FeatureTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 18/1/2019.
//  Copyright © 2019 Don Chu. All rights reserved.
//

import UIKit

class FeatureTableViewCell: UITableViewCell {
    @IBOutlet weak var featureImg: UIImageView!
    @IBOutlet weak var featureLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
