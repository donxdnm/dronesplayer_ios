//
//  ProductSectionTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 30/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class ProductSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productLB: UILabel!
    @IBOutlet weak var goToProductDetailBtn: PassableUIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
