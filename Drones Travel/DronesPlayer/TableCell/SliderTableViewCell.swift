//
//  SliderTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 26/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import FSPagerView

class SliderTableViewCell: UITableViewCell {
     var selectDelegate:MyCustomCellDelegator!
    @IBOutlet weak var sliderView: FSPagerView!{
        didSet {
            self.sliderView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    var sliderAr:[Slider]? = nil
    @IBOutlet weak var sliderControl: FSPageControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SliderTableViewCell :FSPagerViewDelegate{
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if (sliderAr?[index].post_id != nil){
            
            self.selectDelegate.callSegueFromCell( id: (sliderAr?[index].post_id)!)
        }else if(sliderAr?[index].url != nil){
            self.selectDelegate.callSegueFromCell(url: (sliderAr?[index].url)!)
        }
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.sliderControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.sliderControl.currentPage = pagerView.currentIndex
    }
    
    
}
