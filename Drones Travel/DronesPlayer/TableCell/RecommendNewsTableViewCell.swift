//
//  RecommendNewsTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 30/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class RecommendNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsTable: UITableView!
    @IBOutlet weak var sectionTitleLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        sectionTitleLB.text = NSLocalizedString("推薦文章", comment: "")
        newsTable.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        newsTable.rowHeight = UITableView.automaticDimension;
        newsTable.estimatedRowHeight = 120.0;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.newsTable.layoutSubviews()
        //        self.superTableView?.beginUpdates()
        //        self.superTableView?.endUpdates()
    }

}
