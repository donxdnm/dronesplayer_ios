//
//  NewsSectionTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 26/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class NewsSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var indexNewsTable: NestedInnerTableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        indexNewsTable.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        indexNewsTable.rowHeight = UITableView.automaticDimension;
        indexNewsTable.estimatedRowHeight = 15.0;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.indexNewsTable.layoutSubviews()
//        self.superTableView?.beginUpdates()
//        self.superTableView?.endUpdates()
    }

}
