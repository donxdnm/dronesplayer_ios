//
//  SpecTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 7/11/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class SpecTableViewCell: UITableViewCell {

    @IBOutlet weak var specNameLB: UILabel!
    @IBOutlet weak var specValueLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
