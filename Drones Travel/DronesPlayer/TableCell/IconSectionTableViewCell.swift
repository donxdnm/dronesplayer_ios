//
//  IconSectionTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 26/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class IconSectionTableViewCell: UITableViewCell {
    var selectDelegate:MyCustomCellDelegator!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func sectionBBtn1Clicked(_ sender: Any) {
        
        let instagramHooks = "instagram://user?username=dronesplayer"
        let instagramUrl = NSURL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl! as URL)
        {
            UIApplication.shared.openURL(instagramUrl! as URL)
            
        } else {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.openURL(NSURL(string: "http://instagram.com/")! as URL)
        }
    }
    @IBAction func sectionBBtn2Clicked(_ sender: Any) {
        selectDelegate.toWebViewfromCell(name: "specialoffer/")
    }
    @IBAction func sectionBBtn3Clicked(_ sender: Any) {
        let instagramHooks = "fb://page/?id=1192724790808873"
        let instagramUrl = NSURL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl! as URL)
        {
            UIApplication.shared.openURL(instagramUrl! as URL)
            
        } else {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/%E8%88%AA%E6%8B%8D%E6%A9%9F%E6%90%9C%E6%95%91%E6%9C%8D%E5%8B%99-Drone-Rescue-Service-1192724790808873/")! as URL)
        }
    }
    @IBAction func sectionBBtn4Clicked(_ sender: Any) {
        selectDelegate.toWebViewfromCell(name: "recruitment/")
    }
    @IBAction func sectionBBtn5Clicked(_ sender: Any) {
    }

    @IBAction func sectionBBtn7Clicked(_ sender: Any) {
        selectDelegate.toWebViewfromCell(name: "businesscooperation/")
        
    }
    @IBAction func sectionBBtn8Clicked(_ sender: Any) {
    }
}
