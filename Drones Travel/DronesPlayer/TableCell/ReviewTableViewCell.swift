//
//  ReviewTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 29/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var tagLB: UILabel!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var authorLB: UILabel!
    @IBOutlet weak var dateLB: UILabel!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowRadius = 2.0
        containerView.layer.shadowOpacity = 0.3
        containerView.layer.shadowOffset = CGSize.init(width: 0.0, height: 2.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
