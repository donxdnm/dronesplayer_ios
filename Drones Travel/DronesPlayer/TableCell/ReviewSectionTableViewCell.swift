//
//  ReviewSectionTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 29/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class ReviewSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var indexReviewTable: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        indexReviewTable.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "reviewCell")
        indexReviewTable.rowHeight = UITableView.automaticDimension;
        indexReviewTable.estimatedRowHeight = 120.0;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.indexReviewTable.layoutSubviews()
//        self.superTableView?.beginUpdates()
//        self.superTableView?.endUpdates()
    }
}
