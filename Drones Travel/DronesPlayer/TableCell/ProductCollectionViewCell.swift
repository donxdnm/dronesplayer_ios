//
//  ProductCollectionViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 6/11/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productLB: UILabel!
    @IBOutlet weak var rateLB: UILabel!
    
}
