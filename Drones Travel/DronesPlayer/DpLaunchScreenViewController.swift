//
//  DpLaunchScreenViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 24/10/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import ESTabBarController_swift
import NVActivityIndicatorView
import Alamofire

class DpLaunchScreenViewController: UIViewController {

    @IBOutlet weak var dp_logo: UIImageView!
    @IBOutlet weak var loading: NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loading.startAnimating()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        fetchIndexData()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func fetchIndexData(){
        Constants.indexResponse = nil
        let parameters: [String: String] = [
            "action" : "get-index-section"
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<IndexResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let indexResponse = response.result.value
                print("Validation Successful")
                
                if indexResponse != nil{
                    Constants.indexResponse = indexResponse
                    self.goToIndex()
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "indexResponseReady"), object: nil)
                break
            case .failure(let error):
                print(error)
                let errorAlert = Functions.showNetworkErrorAlert {
                    self.fetchIndexData()
                }
                errorAlert.show()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "indexResponseFetchFail"), object: nil)
                break
            }
        }
    }
    func goToIndex(){
        let tabBarController = ESTabBarController()
        if let tabBar = tabBarController.tabBar as? ESTabBar {
            tabBar.tintColor = Constants.selectedColor
        }
        tabBarController.delegate = nil
        tabBarController.tabBar.shadowImage = UIImage(named: "transparent")
        tabBarController.tabBar.backgroundImage = UIImage.from(color: .black)
        tabBarController.shouldHijackHandler = {
            tabbarController, viewController, index in
            if index == 2 {
                return true
            }
            return false
        }
        tabBarController.didHijackHandler = {
            [weak tabBarController] tabbarController, viewController, index in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
//                let takePhotoAction = UIAlertAction(title: "Take a photo", style: .default, handler: nil)
//                alertController.addAction(takePhotoAction)
//                let selectFromAlbumAction = UIAlertAction(title: "Select from album", style: .default, handler: nil)
//                alertController.addAction(selectFromAlbumAction)
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                alertController.addAction(cancelAction)
//                tabBarController?.present(alertController, animated: true, completion: nil)
                let viewController:LandingViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "landingView") as! LandingViewController
                tabBarController?.present(viewController, animated: true, completion: nil)
                //self.show(viewController, sender: self)
//                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        
        let v1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DpIndex") as UIViewController
        let v2 = DpNewsListViewController()
        let v3 = DpIndexViewController()
        let v4 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productList") as UIViewController
        let v5 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingPage") as UIViewController
        
        v1.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: "首頁", image: UIImage(named: "home_btn"), selectedImage: UIImage(named: "home__btn_selected"))
        v2.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: "資訊", image: UIImage(named: "news_btn"), selectedImage: UIImage(named: "news_btn_selected"))
        let centerTabbarItemImage = UIImage(named: "tools_btn")?.withRenderingMode(.alwaysOriginal)
        
        let centerTabbarItem = ESTabBarItem.init(ExampleIrregularityContentView(),title: nil, image: centerTabbarItemImage, selectedImage: nil)
        
        v3.tabBarItem = centerTabbarItem
        
        v4.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: "機體", image: UIImage(named: "drone_btn"), selectedImage: UIImage(named: "drone_btn_selected"))
        v5.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: "設定", image: UIImage(named: "setting_btn"), selectedImage: UIImage(named: "setting_btn_selected"))
        
        tabBarController.viewControllers = [v1, v2, v3, v4, v5]
        
        let navigationController = DpNavigationController.init(rootViewController: tabBarController)
        self.present(navigationController, animated: true, completion: nil)
    }
}
