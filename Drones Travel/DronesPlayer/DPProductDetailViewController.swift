//
//  DPProductDetailViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 7/11/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import WebKit
import SDWebImage
import FSPagerView
import Alamofire

class DPProductDetailViewController: UIViewController {
    @IBOutlet weak var sliderView: FSPagerView! {
        didSet {
            self.sliderView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    @IBOutlet weak var sliderControl: FSPageControl!
    @IBOutlet weak var produnctNameLB: UILabel!
    
    @IBOutlet weak var ratingLB: UILabel!
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var specTV: NestedInnerTableView!
    @IBOutlet weak var specTableTitleLB: UILabel!
    @IBOutlet weak var specTVHeight: NSLayoutConstraint!
    @IBOutlet weak var relatedPostTV: NestedInnerTableView!
    @IBOutlet weak var featureTV: UITableView!
    @IBOutlet weak var featureTVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var contentSV: UIScrollView!
    
    @IBOutlet weak var webHeight: NSLayoutConstraint!
    @IBOutlet weak var relatedProductTitle: UIView!
    var contentWV:WKWebView? = nil
    var postID = "0"
    var postDetail :Drone? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if postID != "0" {
            self.fetchProduct(postID: postID)
        }
        contentSV.delegate = self
        // Do any additional setup after loading the view.
    }
    
    deinit {
        do{
            contentWV?.scrollView.removeObserver(self, forKeyPath: "contentSize")
        }catch{
            //do nothing, obviously it wasn't attached because an exception was thrown
        }
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    func setupView(post:Drone){
        setupSlider(sliderView: sliderView, sliderControl: sliderControl)
        produnctNameLB.text = postDetail?.name
        if postDetail?.name != nil || (postDetail?.name)! == ""{
            specTableTitleLB.text = "無人機規格"
        }else{
            specTableTitleLB.text = (postDetail?.name)! + "-" + "無人機規格"
        }
        
        if postDetail?.name != nil{
            Functions.sendGATrack(viewName: "drones - " + (postDetail?.name)!)
        }else{
            Functions.sendGATrack(viewName: "drones - nameisnull")
        }
        
        ratingLB.text = postDetail?.rating
        if postDetail?.content != nil{
            let webView = WKWebView()
            //            webView.uiDelegate as! WKUIDelegate = self
            webView.translatesAutoresizingMaskIntoConstraints=false
            //            cell.containerV.translatesAutoresizingMaskIntoConstraints=false
            webViewContainer.addSubview(webView)
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.loadHTMLString(post.content!,baseURL: nil)
            webView.leadingAnchor.constraint(equalTo: webViewContainer.leadingAnchor, constant: 0).isActive = true
            
            webView.trailingAnchor.constraint(equalTo: webViewContainer.trailingAnchor, constant: 0).isActive = true
            //                webView.scrollView.isScrollEnabled = false
            webView.topAnchor.constraint(equalTo: webViewContainer.topAnchor, constant: 0).isActive = true
            //                webView.uiDelegate = self
            //                webView.navigationDelegate = self
            webView.bottomAnchor.constraint(equalTo: webViewContainer.bottomAnchor, constant: 0).isActive = true
            contentWV = webView
            webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
            
        }
        specTV.dataSource = self
        specTV.delegate = self
        specTV.estimatedSectionHeaderHeight = 120.0
        specTV.sectionHeaderHeight = UITableView.automaticDimension
        specTV.estimatedRowHeight = 120.0
        specTV.rowHeight = UITableView.automaticDimension
        specTV.reloadData()
        
        featureTV.delegate = self
        featureTV.dataSource = self
        featureTV.reloadData()
        
        if postDetail?.related_post?.count == 0{
            relatedProductTitle.isHidden = true
        }else{
            relatedProductTitle.isHidden = false
        }
        relatedPostTV.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        relatedPostTV.delegate = self
        relatedPostTV.dataSource = self
        relatedPostTV.estimatedRowHeight = 120.0
        relatedPostTV.rowHeight = UITableView.automaticDimension
        relatedPostTV.reloadData()
    }
    
    func setPostID(postID: String) {
        self.postID = postID
    }
    
    func setupSlider(sliderView:FSPagerView, sliderControl: FSPageControl) {
        sliderView.reloadData()
        
        sliderControl.numberOfPages = self.postDetail?.images?.count ?? 0
        sliderControl.contentHorizontalAlignment = .center
        sliderControl.setFillColor(.gray, for: .normal)
        sliderControl.setFillColor(Constants.selectedColor, for: .selected)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"{
            let contentHeight = contentWV?.scrollView.contentSize.height
            webHeight.constant = contentHeight!
        }
    }
    
    func fixTableViewInsets() {
        let zContentInsets = UIEdgeInsets.zero
        contentSV.contentInset = zContentInsets
        contentSV.scrollIndicatorInsets = zContentInsets
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        fixTableViewInsets()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DPProductDetailViewController{
    func fetchProduct(postID: String) {
        let parameters: [String: String] = [
            "action" : "get-drones-details",
            "id" : postID
        ]
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<DronesResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let postDetailResponse = response.result.value as! DronesResponse
                
                print("Validation Successful")
                
                if postDetailResponse != nil && postDetailResponse.data != nil{
                    self.postDetail = postDetailResponse.data!
                    self.setupView(post: postDetailResponse.data!)
                    
                }else{
                    print("error")
                }
                
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}

extension DPProductDetailViewController: FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return postDetail?.images?.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let imgUrl = URL.init(string: (postDetail?.images?[index].url)!)
        cell.imageView?.sd_setImage(with: imgUrl)
        cell.imageView?.contentMode = .scaleAspectFit
        return cell
    }
    
    
    
}

extension DPProductDetailViewController:FSPagerViewDelegate{
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.sliderControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.sliderControl.currentPage = pagerView.currentIndex
    
    }
    
    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
        return false
    }
}

extension DPProductDetailViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == relatedPostTV {
            let viewController = Functions.getPostDetailViewControler(post_id: (self.postDetail?.related_post?[indexPath.row].id)!)
                

                self.navigationController?.pushViewController(viewController, animated: true)
            
        }else if tableView == specTV || tableView == featureTV{
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == specTV && postDetail != nil{
            let numberOfSection = postDetail?.specs_data?.count ?? 0
            return numberOfSection
        }else if tableView == relatedPostTV || tableView == featureTV{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == specTV {
            return postDetail?.specs_data?[section].data?.count ?? 0
        }else if tableView == relatedPostTV{
            return postDetail?.related_post?.count ?? 0
        }else if tableView == featureTV{
            featureTVHeight.constant = CGFloat((postDetail?.features?.count ?? 0) * 50)
            return postDetail?.features?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == specTV {
            let view = UIView.init(frame: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: tableView.frame.size.width, height: 40)))
            let label = UILabel.init(frame:CGRect.init(x: 0, y: 5, width: tableView.frame.size.width, height: 35))
            label.font = UIFont.boldSystemFont(ofSize: 14)
            label.backgroundColor = UIColor(red:0.65, green:0.66, blue:0.67, alpha:1.0)
            label.textAlignment = .center
            label.textColor = .white
            let title = postDetail?.specs_data?[section].name
            
            label.text = title
            view.addSubview(label)
            return view
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == specTV{
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if tableView == specTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "specCell") as! SpecTableViewCell
            let spec = postDetail?.specs_data![indexPath.section].data![indexPath.row]
            cell.specNameLB.text = spec?.name
            cell.specValueLB.text = spec?.value
            tableView.invalidateIntrinsicContentSize()
            return cell
        }else if tableView == relatedPostTV{
            let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell") as! NewsTableViewCell
            let coverAr = postDetail?.related_post
            let imgUrl = URL.init(string: ((coverAr?[indexPath.row].cover)!))
            let tags = coverAr?[indexPath.row].tags
            var tagString = ""
            if ( (tags?.count)! > 0){
                for tag in tags!{
                    let tagName = tag.name!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                    tagString += " ・"
                    tagString += tagName
                }
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            newsCell.coverImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
            newsCell.titleLB.text = coverAr?[indexPath.row].title!
            newsCell.authorLB.text = coverAr?[indexPath.row].author?.name!
            newsCell.tagLB.text = tagString
            if  (coverAr?[indexPath.row].date) != nil {
                newsCell.dateLB.text = dateFormatter.string(from: (coverAr?[indexPath.row].date)!)
            }
            return newsCell
        }else if tableView == featureTV{
            let featureCell = tableView.dequeueReusableCell(withIdentifier: "featureCell") as! FeatureTableViewCell
            switch postDetail?.features?[indexPath.row].icon{
            case "focus-2":
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .plusSquare, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
                
            case "hdd":
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .hdd, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
            case "speedometer-1":
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .tachometerAlt, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
            case "wifi":
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .wifi, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
            case "upload":
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .upload, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
            case "camera":
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .camera, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
            default:
                featureCell.featureImg.image = UIImage.fontAwesomeIcon(name: .plusSquare, style: .solid, textColor: .white, size: CGSize.init(width: 30, height: 30))
            }
            featureCell.featureImg.backgroundColor = UIColor(red:0.65, green:0.66, blue:0.67, alpha:1.0)
            if postDetail?.features?[indexPath.row].value != nil && postDetail?.features?[indexPath.row].value != ""{
                featureCell.featureLB.numberOfLines = 0
                featureCell.featureLB.font = featureCell.featureLB.font.withSize(10)
                featureCell.featureLB?.text = (postDetail?.features?[indexPath.row].name)! + ":" + (postDetail?.features?[indexPath.row].value)!
                featureCell.featureLB.sizeToFit()
            }
            return featureCell
            
        }
        return cell
    }
    
    

    
    
}

extension DPProductDetailViewController:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pan = scrollView.panGestureRecognizer
        let velocity = pan.velocity(in: scrollView).y
        if velocity < -5 {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            //            self.navigationController?.setToolbarHidden(true, animated: true)
        } else if velocity > 5 {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            //            self.navigationController?.setToolbarHidden(false, animated: true)
        }
    }
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

