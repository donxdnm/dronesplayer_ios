//
//  NewsSearchViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 9/11/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import ESPullToRefresh
import Alamofire
import FontAwesome_swift

class NewsSearchViewController: UIViewController,NVActivityIndicatorViewable{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultTV: UITableView!
    
    @IBOutlet weak var backBtn: UIButton!
    var newsAr:[Cover] = [Cover]()
    var page = 1
    var keyword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if keyword != ""{
            searchBar.text = keyword
            requestNews(keyword: keyword)
        }
        searchResultTV.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        searchResultTV.es.addInfiniteScrolling {
            self.page += 1
            self.requestNews(keyword: self.keyword)
        }
        backBtn.titleLabel?.font = UIFont.fontAwesome(ofSize: 15, style: .solid)
        backBtn.setTitle(String.fontAwesomeIcon(name: .arrowLeft), for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Functions.sendGATrack(viewName: "search - " + keyword)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewsSearchViewController{
    func requestNews(keyword:String){
        let parameters: [String: String] = [
            "action" : "search-post-list",
            "page" : String(page),
            "keyword"  : keyword
        ]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: NSLocalizedString("搜索中⋯⋯", comment: ""), type: NVActivityIndicatorType(rawValue: 6)!)
        let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<PostResponse>) in
            print("The status code is : \(response.response?.statusCode)")
            print("the response is : \(response)")
            switch response.result {
            case .success:
                let moreNewsResponse = response.result.value as! PostResponse
                print("Validation Successful")
                
                if moreNewsResponse != nil && moreNewsResponse.data?.count ?? 0 > 0{
                    self.newsAr += moreNewsResponse.data!
                    self.searchResultTV.reloadData()
                    self.searchResultTV.es.stopLoadingMore()
                }else{
                    self.searchResultTV.es.stopLoadingMore()
                    self.searchResultTV.es.noticeNoMoreData()
                }
                
                break
            case .failure(let error):
                print(error)
                self.searchResultTV.es.stopLoadingMore()
                self.searchResultTV.es.noticeNoMoreData()
                break
            }
            self.stopAnimating()
        }
    }
}


extension NewsSearchViewController:UITableViewDelegate,UITableViewDataSource{
    

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(Functions.getPostDetailViewControler(post_id: newsAr[indexPath.row].id!), animated: true)
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
     func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if newsAr.count != nil && newsAr.count > 0{
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }else{
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "沒有搜尋到任何相關的文章"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsAr.count ?? 0
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let imgUrl = URL.init(string: ((newsAr[indexPath.row].cover)!))
        let tags = newsAr[indexPath.row].tags
        var tagString = ""
        if ( (tags?.count)! > 0){
            for tag in tags!{
                let tagName = tag.name!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                tagString += " ・"
                tagString += tagName
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        guard  let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as? NewsTableViewCell  else {
            return UITableViewCell() }
        newsCell.coverImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder"))
        newsCell.titleLB.text = newsAr[indexPath.row].title!
        newsCell.authorLB.text = newsAr[indexPath.row].author?.name!
        newsCell.tagLB.text = tagString
        if newsAr[indexPath.row].date != nil{
            newsCell.dateLB.text = dateFormatter.string(from: (newsAr[indexPath.row].date)!)
        }
        
        
        return newsCell
    }
}
