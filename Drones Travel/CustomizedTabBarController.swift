//
//  CustomizedTabBarController.swift
//  Drones Travel
//
//  Created by Don Chu on 19/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class CustomizedTabBarController: UITabBarController {
    
    let kBarHeight = 70
    var viewFrameHeight :CGFloat = 0
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        var tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
        
        if #available(iOS 11.0, *) {
             let window = UIApplication.shared.keyWindow
            viewFrameHeight = self.view.frame.size.height - (window?.safeAreaInsets.bottom)!
            tabFrame.size.height = CGFloat(kBarHeight) + (window?.safeAreaInsets.bottom)!
        }else{
            viewFrameHeight = self.view.frame.size.height
            tabFrame.size.height = CGFloat(kBarHeight)
        }
        tabFrame.origin.y =  viewFrameHeight - CGFloat(kBarHeight)
        self.tabBar.frame = tabFrame;
        self.tabBar.tintColor = UIColor.white
        self.tabBar.layoutIfNeeded()
        
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        for item in self.tabBar.items!{
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        self.tabBar.barTintColor = Constants.primaryColor
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor.white
        backItem.title = NSLocalizedString("返回", comment: "")
        navigationItem.backBarButtonItem = backItem
        // Do any additional setup after loading the view.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setSelectedIndex(index:Int){
        self.selectedIndex = index
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
