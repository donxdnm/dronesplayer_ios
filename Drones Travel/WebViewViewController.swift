//
//  webViewViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 17/8/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class WebViewViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    var webView: WKWebView!
    var url:String?
    var staticPageType:StaticPageCase = StaticPageCase.nonStaticPage
    enum StaticPageCase :String{
        case aboutUS = "aboutus-data"
        case privacy = "privacy-data"
        case service = "terms-data"
        case nonStaticPage = ""
    }
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if staticPageType == .nonStaticPage{
            loadUrl()
        }else{
            loadStaticPage()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            if url.absoluteString.contains("share") || url.scheme == "whatsapp" || url.scheme == "mailto"{
                    print(url.host)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                        decisionHandler(.cancel)
                        return
                    }
               
            }
            
            decisionHandler(.allow)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebViewViewController{
    func loadUrl (){
        var thisUrl:URL? = nil
        if url != nil{
            thisUrl = URL.init(string: url!)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        print(url)
        let myRequest = URLRequest(url: thisUrl!)
        webView.load(myRequest)
    }
    
    func loadStaticPage (){
        if staticPageType != .nonStaticPage
        {
            let parameters: [String: String] = [
                "action" : staticPageType.rawValue
            ]
            let url = Constants.apiDomain + "wp-admin/admin-ajax.php"
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<StaticPageContent>) in
                print("The status code is : \(response.response?.statusCode)")
                print("the response is : \(response)")
                switch response.result {
                case .success:
                    let contentData = response.result.value as! StaticPageContent
                    
                    print("Validation Successful")
                    
                    if contentData != nil && contentData.content != nil{
                        self.webView.loadHTMLString(contentData.content!, baseURL: nil)
                        
                    }else{
                        print("error")
                    }
                    
                    break
                case .failure(let error):
                    let errorAlert = Functions.showNetworkErrorAlert {
                        self.loadStaticPage()
                    }
                    errorAlert.show()
                    print(error)
                    break
                }
            }
        }

    }
}

