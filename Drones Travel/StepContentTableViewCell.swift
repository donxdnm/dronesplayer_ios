//
//  StepContentTableViewCell.swift
//  Drones Travel
//
//  Created by Don Chu on 22/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import SDWebImage

class StepContentTableViewCell: UITableViewCell {

    @IBOutlet weak var stepNumLB: UILabel!
    @IBOutlet weak var subjectLB: UILabel!
    @IBOutlet weak var contentLB: UILabel!
    @IBOutlet weak var relatedLinkBtn: UIButton!
    @IBOutlet weak var bottonIndicator: UILabel!
    @IBOutlet weak var attectedImgIV: UIImageView?
    @IBOutlet var attectedImgHeight: NSLayoutConstraint!

    var relatedUrl: String = ""
    var imgUrl: String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
//        bottonIndicator?.font = UIFont.fontAwesome(ofSize: 20)
//       bottonIndicator.text =  String.fontAwesomeIcon(name: .sortDown)
        if relatedUrl == "" {
            relatedLinkBtn.isHidden = true
        }else{
            relatedLinkBtn.isHidden = false
        }
        if imgUrl != ""{
            attectedImgIV?.isHidden = false
            setUpImgwithUrl(url: imgUrl)
        }
        stepNumLB.text = String.fontAwesomeIcon(name: .circle)
        stepNumLB?.font = UIFont.fontAwesome(ofSize: 20, style: .brands)
        // Initialization code
    }
    func setupRelatedUrl(url:String = ""){
        relatedUrl = url
        if relatedUrl == "" {
            relatedLinkBtn.isHidden = true
        }else{
            relatedLinkBtn.isHidden = false
        }

    }
    
    func setImgUrl(url:String = "", tableView tableview:UITableView){
        if url == ""{
            if imgUrl != ""{
                self.setUpImgwithUrl(url: imgUrl)
            }
        }else{
            imgUrl = url
            self.setUpImgwithUrl(url: imgUrl)
        }
    }
    
    func setUpImgwithUrl(url:String){
//        attectedImgIV?.isHidden = false
//        attectedImgHeight.isActive = false
        let imgUrl = URL.init(string: url)
//        self.attectedImgHeight.constant = Functions.calculateEstimateImgHeightWithWidth(width: (self.attectedImgIV?.frame.width)!, imgObject: UIImage.init(named: "placeholder")!)
//        self.layoutIfNeeded()
        attectedImgIV?.sd_setImage(with: imgUrl!, placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//            let imgHeight = Functions.calculateEstimateImgHeightWithWidth(width: (self.attectedImgIV?.frame.width)!, imgObject: image!)
//            self.attectedImgHeight.constant = imgHeight
//            self.layoutIfNeeded()
        })
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func relatedLinkBtnClicked(_ sender: Any) {
        if let url = URL(string: relatedUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
}
