
//
//  ViewController.swift
//  Drones Travel
//
//  Created by Don Chu on 15/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import UIKit
import Mapbox
import FontAwesome_swift
import NVActivityIndicatorView

class DroneLimitationZoneViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var userLocationBtn: UIButton!
    @IBOutlet weak var backToOriginBtn: UIButton!
    @IBOutlet weak var layerBtn: UIButton!


    var mapView: MGLMapView!
    var layers: [MGLStyleLayer] = []
    var layersFromUs: [MGLStyleLayer] = []
    var zoneTableIsCollapse = false
    var zoneTableFooterHeight = 30
    var zoneTableHeaderContainerHeight = 65
    var zoneTableHeaderHeight = 25
    var countryName : String = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mapboxUrlForSelectedCountry = Constants.selectedCountryObj?.mapbox_style_url
        let styleURL = URL(string: mapboxUrlForSelectedCountry!)
        mapView = MGLMapView(frame: view.bounds, styleURL:styleURL)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let countryCoordinate = CLLocationCoordinate2D.init(latitude: Constants.selectedCountryObj?.latitude as! CLLocationDegrees, longitude: Constants.selectedCountryObj?.longitude as! CLLocationDegrees)
        let sealevel:Double = (Constants.selectedCountryObj?.zoomlevel)!
        mapView.setCenter(countryCoordinate, zoomLevel:sealevel, animated: false)
        mapView.delegate = self
        
        view.insertSubview(mapView, at: 0)
        
        mapView.delegate = self
        mapView.showsHeading = true
        mapView.showsUserLocation = true
        mapView.logoView.isHidden = true
        mapView.tintColor = UIColor(red:0.98, green:0.51, blue:0.20, alpha:1.00)
        
        mapView.attributionButton.isHidden = true
        
        if (Constants.layerAr.count) > 0 {
            stopNavIndication()
        }else{
            stopNavIndication()
            showNetworkError()
            NotificationCenter.default.addObserver(self, selector: #selector(self.stopNavIndication), name: NSNotification.Name(rawValue: "layerStepArReady"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.showNetworkError), name: NSNotification.Name(rawValue: "layerStepArFetchFail"), object: nil)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Functions.sendGATrack(viewName: (Constants.selectedCountryObj?.name)! + "禁飛區")
    }
    // Wait until the style is loaded before modifying the map style
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {

        layers = (mapView.style?.layers)!;
        
        
        for layerInfo in Constants.layerAr {
            for layer in layers {
                if (layer.identifier == layerInfo.layer_id){
                    layersFromUs.append( layer)
                }
            }
        }
//        for layerInfo in selectedLayer {
//            let mapUrl = "mapbox://"+layerInfo["map_id"]!
//            let source = MGLVectorTileSource(identifier: layerInfo["source_id"]!, configurationURL: URL(string: mapUrl )!)
//            style.addSource(source)
//            addLayer(to: style, tileSource: source, layerID: layerInfo["id"]!)
//        }
    }
    
//    func addLayer(to style: MGLStyle, tileSource source:MGLVectorTileSource, layerID ID:String) {
//
//        let layer = MGLCircleStyleLayer(identifier: ID, source: source)
//        layer.sourceLayerIdentifier = ID
//        layersFormUs.append( layer)
//        style.addLayer(layer)
//    }

    @IBAction func popupLayerPicker(_ sender: Any) {
        performSegue(withIdentifier: "layerPopup", sender:layersFromUs)
    }
    
    func showLayer(layer: MGLStyleLayer) {
        layer.isVisible = true
    }
    
    func hideLayer(layer: MGLStyleLayer) {
        layer.isVisible = false
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        let camera = MGLMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 4000, pitch: 0, heading: 0)
        mapView.fly(to: camera, completionHandler: nil)
    }


   

    
    @IBAction func userLocationBtnClicked(_ sender: Any) {
        let camera = MGLMapCamera(lookingAtCenter: (mapView.userLocation?.coordinate)!, fromDistance: 4000, pitch: 0, heading: 0)
        mapView.fly(to: camera, completionHandler: nil)
    }
    @IBAction func zoomBtnClicked(_ sender: Any) {
        
        mapView.zoomLevel += 1
    }
    
    @IBAction func unzoomBtnClicked(_ sender: Any) {
        mapView.zoomLevel -= 1
    }

    @IBAction func backToOriginBtnClicked(_ sender: Any) {
        let countryCoordinate = CLLocationCoordinate2D.init(latitude: Constants.selectedCountryObj?.latitude as! CLLocationDegrees, longitude: Constants.selectedCountryObj?.longitude as! CLLocationDegrees)
        let sealevel:Double = (Constants.selectedCountryObj?.zoomlevel)!
        mapView.setCenter(countryCoordinate, zoomLevel:sealevel, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "layerPopup"{
            let destinationVC = (segue.destination as! LayerPickerViewController)
            destinationVC.layers = layersFromUs
            destinationVC.layerInfo = Constants.layerAr
        }
    }
    
}

extension DroneLimitationZoneViewController:MGLMapViewDelegate{
//    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
//        // Substitute our custom view for the user location annotation. This custom view is defined below.
//        if annotation is MGLUserLocation && mapView.userLocation != nil {
//            return CustomUserLocationAnnotationView()
//        }
//        return nil    }
}

extension DroneLimitationZoneViewController{

    
    func setUpCircularImg(imgView: UIImageView){
        imgView.layer.borderWidth = 1
        imgView.layer.masksToBounds = false
        imgView.layer.borderColor = UIColor.black.cgColor
        imgView.layer.cornerRadius = imgView.frame.height/2
        imgView.clipsToBounds = true
    }
    

    
}

extension DroneLimitationZoneViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if zoneTableIsCollapse {
            return 0
        }else{
            return Constants.layerAr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! LayerTableViewCell
        cell.layerName.text = Constants.layerAr[indexPath.row].layer_name
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if layersFromUs.count > indexPath.row {
            let layer = layersFromUs[indexPath.row]
            
            if (layer.isVisible) {
                self.hideLayer(layer: layer)
            }else{
                self.showLayer(layer: layer)
            }
        }
        
    }
    
    
}

extension DroneLimitationZoneViewController{
    @objc func stopNavIndication(){
        stopAnimating()
        mapView.reloadStyle(nil)
    }
    
    @objc func showNetworkError(){
        let errorAlert = Functions.showNetworkErrorAlert {
            IndexViewController.fetchLayerFromApi(id: (Constants.selectedCountryObj?.id!)!)
        }
        errorAlert.show()
    }
    
    
}

class CustomUserLocationAnnotationView: MGLUserLocationAnnotationView {
    let size: CGFloat = 20
    var dot: CALayer!
    var arrow: CAShapeLayer!
    
    // -update is a method inherited from MGLUserLocationAnnotationView. It updates the appearance of the user location annotation when needed. This can be called many times a second, so be careful to keep it lightweight.
    override func update() {
        if frame.isNull {
            frame = CGRect(x: 0, y: 0, width: size, height: size)
            return setNeedsLayout()
        }
        
        // Check whether we have the user’s location yet.
        if CLLocationCoordinate2DIsValid(userLocation!.coordinate) {
            setupLayers()
            updateHeading()
        }
    }
    
    private func updateHeading() {
        // Show the heading arrow, if the heading of the user is available.
        if let heading = userLocation!.heading?.trueHeading {
            arrow.isHidden = false
            
            // Get the difference between the map’s current direction and the user’s heading, then convert it from degrees to radians.
            let rotation: CGFloat = -MGLRadiansFromDegrees(mapView!.direction - heading)
            
            // If the difference would be perceptible, rotate the arrow.
            if fabs(rotation) > 0.01 {
                // Disable implicit animations of this rotation, which reduces lag between changes.
                CATransaction.begin()
                CATransaction.setDisableActions(true)
                arrow.setAffineTransform(CGAffineTransform.identity.rotated(by: rotation))
                CATransaction.commit()
            }
        } else {
            arrow.isHidden = true
        }
    }
    
    private func setupLayers() {
        // This dot forms the base of the annotation.
        if dot == nil {
            dot = CALayer()
            dot.bounds = CGRect(x: 0, y: 0, width: size, height: size)
            
            // Use CALayer’s corner radius to turn this layer into a circle.
            dot.cornerRadius = size / 2
            //dot color
            dot.backgroundColor = UIColor(red:0.98, green:0.51, blue:0.20, alpha:1.00).cgColor
            dot.borderWidth = 4
            dot.borderColor = UIColor.white.cgColor
            layer.addSublayer(dot)
        }
        
        // This arrow overlays the dot and is rotated with the user’s heading.
        if arrow == nil {
            arrow = CAShapeLayer()
            arrow.path = arrowPath()
            arrow.frame = CGRect(x: 0, y: 0, width: size / 2, height: size / 2)
            arrow.position = CGPoint(x: dot.frame.midX, y: dot.frame.midY)
            arrow.fillColor = dot.borderColor
            layer.addSublayer(arrow)
        }
    }
    
    // Calculate the vector path for an arrow, for use in a shape layer.
    private func arrowPath() -> CGPath {
        let max: CGFloat = size / 2
        let pad: CGFloat = 3
        
        let top =    CGPoint(x: max * 0.5, y: 0)
        let left =   CGPoint(x: 0 + pad,   y: max - pad)
        let right =  CGPoint(x: max - pad, y: max - pad)
        let center = CGPoint(x: max * 0.5, y: max * 0.6)
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: top)
        bezierPath.addLine(to: left)
        bezierPath.addLine(to: center)
        bezierPath.addLine(to: right)
        bezierPath.addLine(to: top)
        bezierPath.close()
        
        return bezierPath.cgPath
    }
}
