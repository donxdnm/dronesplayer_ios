//
//  Drones_TravelTests.swift
//  Drones TravelTests
//
//  Created by Don Chu on 15/6/2018.
//  Copyright © 2018 Don Chu. All rights reserved.
//

import XCTest
@testable import Drones_Travel

class Drones_TravelTests: XCTestCase {
    
    var loginViewController: MemberViewController!
    
    override func setUp() {
        super.setUp()
        loginViewController = MemberViewController()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLogin(){
        var loginSuccess = false
        var autoRelogin = false
        let userName = "donchu"
        let password = "f20110411d"
        Functions.loginApi(accessToken: nil, userName: userName, password: password, useAccessToken: false, complete: {(success:Bool, message:String) in
            loginSuccess = true
            NSLog("%@", message)
        })
        Functions.loginApi(accessToken: Constants.accessToken, userName: userName, password: nil, useAccessToken: true, complete: {(success:Bool, message:String) in
            autoRelogin = true
            NSLog("%@", message)
        })
        
        XCTAssert(loginSuccess && autoRelogin)
        
    }
}
